app.controller('Header', async function($scope, $rootScope, $location,$timeout, API) {

	if (oAuth) {

		$scope.perfilName1 = await API.post("get_perfil");

		$scope.$apply();

		$rootScope.perfilEnable1 = function (name) {
			let isIxion = false;
			if (name == 'subscriber') {
				isIxion = $scope.perfilName1.includes('ixion'); 
			}
			return Boolean($scope.perfilName1.includes(name) + isIxion);
		}
	}


})