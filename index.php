<?php get_header(); ?>
<h1 class="text-center" style="margin-bottom: 30px;">Competence mapping </h1>
<main ng-app="myApp" ng-controller="Login" class="ng-cloak">
	
	<div class="container" style="max-width: 600px; margin-bottom: 22vh;">

		<form name="formLogin" novalidate>
			<br>
			<div class="input-group">
				<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
				<input required="required" ng-model="user.email" type="text" class="form-control" id="email" placeholder="Login / Email">
			</div>
			<br>
			<div class="input-group">
				<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
				<input required="required" ng-model="user.password" type="password" class="form-control" id="pwd" placeholder="Password">
			</div>
			<hr>
			<button type="submit" class="btn pull-right btn-info" ng-click="logar(user, formLogin.$valid)" >Login <i class="glyphicon glyphicon-log-in"></i></button>
		</form>

	</div>

</main>

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/controllers/App.js?v=<?php echo get_rand(); ?>"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/controllers/LoginController.js?v=<?php echo get_rand(); ?>"></script>
<?php get_footer(); ?>