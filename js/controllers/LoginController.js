app.controller('Login', function($scope, $rootScope, $location, $window, $http, API) {
	localStorage.clear();
	$scope.logar = function (user, formOk) {
		if (formOk) {

			API.loading(1);

			$http({
				url: API.urlApiBase + "jwt-auth/v1/token" ,
				method: "POST",
				data: {
					username: user.email,
					password: user.password
				},
			}).then(function mySuccess(r) {
				if ( r.data.token ) {
					localStorage.setItem('oAuth', JSON.stringify(r.data));

					let existRedirect = API.get_params_url("redirect");

					if (existRedirect) {
						$window.location.href = existRedirect;
					}

					else {
						$window.location.href = urlHome+"home/" ;
					}

					API.loading(0);

				} else {

					API.alertar_erro("User or Password are invalid.");
					API.loading(0);
				}
			}, function myError(r) {

				API.alertar_erro("User or Password are invalid.");
				API.loading(0);

			});


			return;
		}

		API.alertar_erro("Fields required.");
	}


});