<?php

namespace App\Endpoints;


class ManagerController
{

	function __construct() {
		
	}

	public static function args()
	{
		return array(
			'ID_competence_item' => array(
				'required' => true,
				'validate_callback' => function($param, $request, $key) {
					return is_numeric( $param );
				}
			),
			'IDs_managers' => array(
				'required' => true,
				'validate_callback' => function($param, $request, $key) {
					return is_array( $param ); 
				}
			),
		);
	}

	
	public static function insert_manager_by_competence($request)
	{	

		global $wpdb;
		$data = array();
		$idCompetenceItem = $request->get_param('ID_competence_item');

		$results = [];

		if ($request->offsetExists('IDs_managers')  ) {

			foreach ($request->get_param('IDs_managers') as $key => $v) {
				$data = array('ID_competence_item' => $idCompetenceItem, 'ID_manager' => $v);
				$results[] = $wpdb->insert("{$wpdb->prefix}competences_itens_managers", $data);
			}
		}


		return array('error' => false, 'message' => 'Sucess!' );
	}

	
	public static function delete_manager_by_competence($request)
	{	

		global $wpdb;
		$data = array();
		$idCompetenceItem = $request->get_param('ID_competence_item');

		$results = [];

		if ($request->offsetExists('IDs_managers')  ) {

			foreach ($request->get_param('IDs_managers') as $key => $v) {
				$where = array('ID_competence_item' => $idCompetenceItem, 'ID_manager' => $v);
				
				$results[] = $wpdb->delete("{$wpdb->prefix}competences_itens_managers", $where, array('%d', '%d'));
			}
		}
		return array('error' => false, 'message' => 'Sucess!' );
	}

}