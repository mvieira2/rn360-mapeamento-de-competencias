app.controller('Admin', function($scope, $rootScope, $location,$timeout, API) {

	$scope.competence_selected = {};
	$scope.competence_field_selected = null;
	$scope.action = null;
	$scope.perfilName = [];
  $scope.filter= { "year" : new Date().getFullYear()};


  $scope.get_competences = async function () {
    $scope.competences = await API.get('competences_itens');
    $scope.$apply();
  }


  $scope.getUnique = function (arr, comp) {

    const unique = arr
    .map(e => e[comp])

     	// store the keys of the unique objects
     	.map((e, i, final) => final.indexOf(e) === i && i)

    	// eliminate the dead keys & store unique objects
    	.filter(e => arr[e]).map(e => arr[e]);

    	return unique;
    }


    $scope.get_count_reports_by_id_user = function (idUser) {
    	return $scope.reports.filter(function (report) {
    		return report.ID_user == idUser
    	}).length
    }


    $scope.init = async function () {
    	await $scope.get_competences();
      await $scope.get_years_available();


      if ($scope.competence_selected.hasOwnProperty("ID")) {
        $scope.competence_selected = $scope.competences.filter(function (comp) {
         return comp.ID == $scope.competence_selected.ID
       })[0];

      }
      else {
        $scope.competence_selected = $scope.competences[0];
      }

      $scope.perfilName = await API.post("get_perfil");

      await $scope.get_managers();
      await $scope.get_administrators();


      $scope.select_competence( $scope.competence_selected );

      $scope.$apply();
    }

    $scope.select_competence = function (c) {
     $scope.competence_field_selected = null;
     $scope.competence_selected = c;


     $scope.managers.map(function (manager) {
      manager.competence_autorized = false;
      return manager;
    }).map(function (manager) {
      if ( !angular.isArray(c.ids_managers_authorized) ) {
        c.ids_managers_authorized = [];
      }
      manager.competence_autorized =  c.ids_managers_authorized.includes( manager.user_nicename );
    });

    $scope.checkAllManagers = $scope.checkIfAllManagers();

  }

  $scope.select_competence_field = function (c) {
   $scope.competence_field_selected = c;
 }

 $scope.isDev = function () {
   return window.location.href.includes("localhost") || window.location.href.includes("?homolog");
 }

 $scope.add = function (action) {
   $scope.modal = null;
   $scope.action = action;

 }
 $scope.modify = function (action) {
   $scope.modal = null;
   if (action == 'modify_competence') {
    $scope.modal = $scope.competence_selected.nameForm;
  }
  if (action == 'modify_competence_field') {
    $scope.modal = $scope.competence_field_selected;
  }

  $scope.action = action;

}
$scope.delete_competence = async function (competence) {

 if (competence.hasOwnProperty("fields") && competence.fields && competence.fields.length) {
  alert(GLOBALS.alerts.msg1);
  return;
}

if (confirm("Confirm delete?")) {
  let result = await API.post("delete_competence_item", { ID : competence.ID });

  if (!result.error) {
   API.alertar_sucesso("Success!");
 }
 $scope.init();
}
}

$scope.delete_competence_field = async function (competence, competence_field_selected) {
 if (confirm("Confirm delete?")) {
  let result = await API.post("delete_competence_field", { ID : competence.ID, field : competence_field_selected });

  if (!result.error) {
   API.alertar_sucesso("Success!");
 }
 $scope.init();
}
}

$scope.save = async function () {
 let result = {};
 switch ($scope.action) {
  case "add_competence":

  result = await API.post("insert_competence", { nameForm : $scope.modal });

  break;
  case "modify_competence":

  result = await API.post("update_competence", { ID: $scope.competence_selected.ID, nameForm : $scope.modal });

  break;
  case "add_competence_field":

  result = await API.post("insert_competence_field", { ID_field: $scope.competence_selected.ID, field : $scope.modal });

  break;
  case "modify_competence_field":

  result = await API.post("update_competence_field", { ID_field: $scope.competence_selected.ID, last_field : $scope.competence_field_selected, field : $scope.modal });

  break;
}

$scope.modal = null;

if (!result.error) {
  API.alertar_sucesso("Success!");
}

$scope.init();

}

$scope.get_managers = async function () {
 $scope.managers = await API.get("managers?order=desc&orderby=date");

 $scope.managers.map(function (manager) {
  manager.competence_autorized = false;
});

 $scope.$apply();
}

$scope.get_administrators = async function () {
  $scope.administrators = await API.get("administrators");


  $scope.$apply();
}


$rootScope.perfilEnable = function (name) {
 return $scope.perfilName.includes(name);
}

$scope.deleteManager = async function (ID) {
 if (confirm("Confirm delete?")) {
  let result = await API.post("create_delete_manager", {"action" : "delete", "ID" : ID});

  if (!result.error) {
   API.alertar_sucesso("Success!");
 }

 $scope.get_managers();
}
}

$scope.addManager = async function (model, isValid) {

 if (!isValid) {
  API.alertar_erro("Fields required");
  return;
}

let result = await API.post("create_delete_manager", {"action" : "create", "login" : model.login, "name" : model.name , "password" : model.password });

if (!result.error) {
  API.alertar_sucesso("Success!");
}

for(k in model){
  model[k] = null;
}

$scope.get_managers();

}
$scope.addAdministrator = async function (model, isValid) {

  if (!isValid) {
    API.alertar_erro("Fields required");
    return;
  }

  let result = await API.post("create_delete_administrator", {"action" : "create", "login" : model.login, "name" : model.name , "password" : model.password });

  if (!result.error) {
    API.alertar_sucesso("Success!");
  }

  for(k in model){
    model[k] = null;
  }

  $scope.get_administrators();

}

$scope.deleteAdministrator = async function (ID) {
  if (confirm("Confirm delete?")) {
    let result = await API.post("create_delete_administrator", {"action" : "delete", "ID" : ID});

    if (!result.error) {
      API.alertar_sucesso("Success!");
    }

    $scope.get_administrators();
  }
}

$scope.get_years_available = function () {    
  let yearInit = [];
  let current_year = new Date().getFullYear();

  yearInit.push( current_year-1, current_year, current_year+1 );

  if (yearInit[0] == 2018) {

    yearInit.shift();

    yearInit.push( current_year+2 );
  }


  $scope.years_available = yearInit.map(function (elem) {
    return {
      year : elem,
      isCurrent : elem === current_year,
    }
  })


}

$scope.filter_setCompetence = function (id) {
  let result = $scope.competences.filter(function (com) {
    return id == com.ID;
  })

  if (result.length) {
    $scope.competences_selecteds = result[0]["fields"];

    return
  }


  $scope.competences_selecteds = [];


}


$scope.search_filter = async function () {


  let result = await API.post('reports', $scope.filter);
  $scope.reports = result.data;

  if (angular.isArray($scope.reports) && $scope.reports.length > 0 ) {
    $scope.reports_header = Object.keys($scope.reports[0]);
  }


  $scope.$apply();

}



$scope.search_filter_180 = async function () {
  let result = await API.post('reports_180', $scope.filter_180);
  $scope.reports_180 = result.data;

  if (angular.isArray($scope.reports_180) && $scope.reports_180.length > 0 ) {
    $scope.reports_180_header = Object.keys($scope.reports_180[0]);
  }

  $scope.$apply();
}

$scope.reset_filter = async function () {
  $scope.reports = null;
  $scope.filter = { "year" : new Date().getFullYear()};
}

$scope.reset_filter_180 = async function () {
  $scope.reports_180 = null;
  $scope.filter_180 = null;
  $scope.reports_180_header = [];
}


$scope.export = function (data) {


/* generate a worksheet */
var ws = XLSX.utils.json_to_sheet(data);

/* add to workbook */
var wb = XLSX.utils.book_new();
XLSX.utils.book_append_sheet(wb, ws, "Reports");

/* write workbook and force a download */
XLSX.writeFile(wb, "reports.xlsx");

}

$scope.checkIfAllManagers = function () {
  return Boolean( $scope.managers.filter( (m) => m.competence_autorized ).length == $scope.managers.length );
}

$scope.call_checkManagers = function (managers) {

  if (managers && managers.length == 1) {
    managers[0].competence_autorized = !managers[0].competence_autorized;
  }

  else {
    let isAllChecked = angular.copy($scope.checkIfAllManagers());

    managers.map(function (manage) {
      manage.competence_autorized = !isAllChecked;
      return manage;
    });

    $scope.checkAllManagers  = $scope.checkIfAllManagers();

  }
}

$scope.saveManagersAuthorizeds = async function () {
  let antigos = $scope.competence_selected.ids_managers_authorized || [];
  let novos = $scope.managers.filter(m => m.competence_autorized  ).map(m => m.user_nicename );

  let removidos = antigos.filter(x => !novos.includes(x));
  let adicionados = novos.filter(x => !antigos.includes(x));


  if (adicionados.length) {
    let result = await API.post("insert_manager_by_competence", { 'ID_competence_item' : $scope.competence_selected.ID,'IDs_managers' : adicionados});

    if (!result.error) {
      API.alertar_sucesso( result.message );
    }
  }

  if (removidos.length) {
    let result = await API.post("delete_manager_by_competence", { 'ID_competence_item' : $scope.competence_selected.ID,'IDs_managers' : removidos})
    if (!result.error) {
      API.alertar_sucesso( result.message );
    }
  }

  $scope.init();

  console.log('removidos')
  console.log(  antigos.filter(x => !novos.includes(x)) )
  console.log('adicionados')
  console.log(  novos.filter(x => !antigos.includes(x)) )

}

$scope.init();

});