var app = angular.module('myApp', []);
var headerApp = angular.module('headerApp', []);
var isDev = window.location.href.includes("localhost");
var urlHome = isDev ? window.location.protocol+"//localhost:83/freelas/rn360-mapeamento-de-competencias/" : window.location.protocol+'//competencemapping.rotativoskf.com.br/homolog/';
var urlAPI = urlHome+ "wp-json/api/v1/";

var oAuth;

try {
	oAuth = JSON.parse(localStorage.getItem('oAuth'));

} catch(e) {
	localStorage.clear();

	oAuth = {};
}
if (oAuth && oAuth.user_display_name) {
	$("#userName").html( (oAuth.user_display_name || 'User') ).attr("title", oAuth.user_display_name);
}
if (oAuth && !oAuth.token ||  location.origin + location.pathname != urlHome && oAuth === null) {
	let param = "?redirect="+window.location.href;

	window.location.href = urlHome + param;

}


app.factory("API", function($http, $rootScope, $timeout){
	var api = {
		urlApiBase : urlHome + "wp-json/",
		urlAPI : urlHome+ "wp-json/api/v1/",
		loading : function (show) {
			if (show) $("#loader").fadeIn('slow');
			if (!show) $("#loader").fadeOut();
		},
		alertar_sucesso : function (msg) {
			$.notify({
				message: msg,
				icon: 'fa fa-check-circle',
			},{
				type:'success',
				icon: 'glyphicon glyphicon-star',
				delay: 1000,
				placement: {
					from: "top",
					align: "center"
				}
			});
		},
		alertar_erro :  function (msg) {
			$.notify({
				message: msg,
				icon: 'glyphicon glyphicon-alert',
			},{
				type:'danger', 
				placement: {
					from: "top",
					align: "center"
				}});
		},
		get_params_url : function (param, url) {
			var url = new URL(url||window.location.href);
			return url.searchParams.get(param)
		},
		call : function (chamada, metodo,parametros, callback ) {
			let self = this;
			self.loading(1);

			if (chamada.includes("?")) {
				chamada += "&lang="+self.get_params_url("lang");
			}

			else {
				chamada += "?lang="+self.get_params_url("lang");
			}

			return $http({
				url: self.urlAPI + chamada,
				method: (metodo||'POST'),
				data: (parametros||null),
				headers: {
					Authorization: 'Bearer '+oAuth.token,
				},
			}).then(function (r) {
				if (typeof callback === 'function') {
					callback(r.data);
				}
				self.loading(0);
				return r.data;

			}, function (error) {
				if (typeof callback === 'function') {
					callback(error.data);
				}
				self.loading(0);

				if (error.hasOwnProperty('message')) {
					console.log(error.message);
					self.alertar_erro( "Not found..." );

				}				
				else if (error.data.hasOwnProperty('message')) {
					self.alertar_erro(error.data.message);

				}
				else {
					self.alertar_erro(error.data);
				}

				// if (location.origin + location.pathname != urlHome) {
				// 	let param = "?redirect="+window.location.href;

				// 	window.location.href = urlHome + param;
				// }

				return error.data;
			});
		},

		export : (function() {
			var uri = 'data:application/vnd.ms-excel;base64,', 
			template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>', 
			base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }, 
			format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
			return function(table, name) {
				if (!table.nodeType) table = document.querySelector(table);

				var dt = new Date();
				var day = dt.getDate();
				var month = dt.getMonth() + 1;
				var year = dt.getFullYear();
				var hour = dt.getHours();
				var mins = dt.getMinutes();
				var sec = dt.getMilliseconds();
				var postfix = year + "-" + month + "-" + day + "-" + hour + "-" + mins+"-"+sec;
				var ctx = {worksheet: name || 'tabela-'+postfix, table: table.innerHTML}

				var link1 = document.createElement("a");
				link1.download = ctx.worksheet+".xls";
				link1.href = uri + base64(format(template, ctx));

				debugger
				link1.click();

				delete link1;

				console.log("planilha")

			}

		})(),


		downloadFile : function (chamada, parametros, callback ) {
			let self = this;
			self.loading(1);
			
			return $http({
				method: 'GET',
				url: self.urlAPI + chamada,
				params: (parametros||null),
				//params: { name: name },
				responseType: 'arraybuffer',
				headers: {
					Authorization: 'Bearer '+oAuth.token,
				},
			}).then(function (r) {

				var data = r.data;
				var headers = r.headers()

				var filename = headers['x-filename'];
				var contentType = headers['content-type'];

				var linkElement = document.createElement('a');


				try {
					var blob = new Blob([data], { type: contentType });
					var url = window.URL.createObjectURL(blob);

					linkElement.setAttribute('href', url);
					linkElement.setAttribute("download", filename);

					var clickEvent = new MouseEvent("click", {
						"view": window,
						"bubbles": true,
						"cancelable": false
					});
					linkElement.dispatchEvent(clickEvent);


					if (typeof callback === "function") {
						callback();
					}

				} catch (ex) {
					console.log(ex);
				}

				self.loading(0);
			})

		}

	}

	return {
		solicitar : api.call,
		get : function(chamada, opts, callback){
			return api.call(chamada, 'GET', opts, callback);
		},
		post : function(chamada,opts, callback){
			return api.call(chamada, 'POST', opts, callback);
		},
		get_params_url: api.get_params_url,	
		alertar_sucesso: api.alertar_sucesso,
		alertar_erro: api.alertar_erro,
		loading: api.loading,
		urlApiBase: api.urlApiBase,
		urlAPI: api.urlAPI,
		export : api.export,
		downloadFile : api.downloadFile
	};

})