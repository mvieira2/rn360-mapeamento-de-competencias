<?php 



require __DIR__ . '/Manager/ManagerController.php';

use App\Endpoints\ManagerController as Manager;



add_action( 'rest_api_init', function ( $server ) {



	register_rest_route( 'api/v1', '/teste1', array(
		'methods'  => 'GET',
		'args' => array(
			'search' => array(
				'required' => true
			)
		),		
		'permission_callback' => function () { return current_user_can( 'administrator' ) || current_user_can( 'manager' )  || current_user_can( 'subscriber' ) || current_user_can( 'ixion' ); } , 
		'callback' => 'teste1',
	));	

	register_rest_route( 'api/v1', '/users', array(
		'methods'  => 'GET',
		'args' => array(
			'search' => array(
				'required' => false
			),
			'year' => array(
				'required' => false,
				'validate_callback' => function($param, $request, $key) {
					return is_numeric( $param );
				}
			),
		),		
		'permission_callback' => function () { return current_user_can( 'administrator' ) || current_user_can( 'manager' )  || current_user_can( 'subscriber' ) || current_user_can( 'ixion' ); } , 
		'callback' => 'users',
	));	

	register_rest_route( 'api/v1', '/managers', array(
		'methods'  => 'GET',
		'permission_callback' => function () { return current_user_can( 'administrator' ) || current_user_can( 'manager' )  || current_user_can( 'subscriber' ) || current_user_can( 'ixion' ); } , 
		'callback' => 'managers',
	));	

	register_rest_route( 'api/v1', '/administrators', array(
		'methods'  => 'GET',
		'permission_callback' => function () { return current_user_can( 'administrator' ) || current_user_can( 'manager' )  || current_user_can( 'subscriber' ); } , 
		'callback' => 'administrators',
	));	

	register_rest_route( 'api/v1', '/get_perfil', array(
		'methods'  => 'POST',
		'permission_callback' => function () { return current_user_can( 'administrator' ) || current_user_can( 'manager' )  || current_user_can( 'subscriber' ) || current_user_can( 'ixion' ); } , 
		'callback' => 'get_perfil',
	));	

	register_rest_route( 'api/v1', '/competences_itens', array(
		'methods'  => 'GET',
		'args' => array(
			'idManager' => array(
				'required' => false,
				'validate_callback' => function($param, $request, $key) {
					return is_numeric( $param );
				}
			),
			'year' => array(
				'required' => false
			)
		),		
		'permission_callback' => function () { return current_user_can( 'administrator' ) || current_user_can( 'manager' )  || current_user_can( 'subscriber' ) || current_user_can( 'ixion' ); } , 
		'callback' => 'competences_itens',
	));	

	register_rest_route( 'api/v1', '/create_user_and_competence', array(
		'methods'  => 'POST',
		'args' => array(
			'user_name' => array(
				'required' => true,
			),
			'email' => array(
				'required' => false,
			),
			'ID' => array(
				'required' => false,
				'validate_callback' => function($param, $request, $key) {
					return is_numeric( $param );
				}
			),

		),		
		'permission_callback' => function () { return current_user_can( 'administrator' ) || current_user_can( 'manager' ) ; } , 
		'callback' => 'create_user_and_competence',
	));	

	register_rest_route( 'api/v1', '/create_delete_manager', array(
		'methods'  => 'POST',
		'args' => array(
			'ID' => array(
				'required' => false,
				'validate_callback' => function($param, $request, $key) {
					return is_numeric( $param );
				}
			),
			'action' => array(
				'required' => false
			),
			'login' => array(
				'required' => false
			),
			'name' => array(
				'required' => false
			),

		),		
		'permission_callback' => function () { return current_user_can( 'administrator' ) || current_user_can( 'manager' ); } , 
		'callback' => 'create_delete_manager',
	));	

	register_rest_route( 'api/v1', '/create_delete_administrator', array(
		'methods'  => 'POST',
		'args' => array(
			'ID' => array(
				'required' => false,
				'validate_callback' => function($param, $request, $key) {
					return is_numeric( $param );
				}
			),
			'action' => array(
				'required' => false
			),
			'login' => array(
				'required' => false
			),
			'name' => array(
				'required' => false
			),

		),		
		'permission_callback' => function () { return current_user_can( 'administrator' ) || current_user_can( 'manager' ); } , 
		'callback' => 'create_delete_administrator',
	));	

	register_rest_route( 'api/v1', '/delete_competence', array(
		'methods'  => 'POST',
		'args' => array(
			'ID' => array(
				'required' => true,
			)
		),		
		'permission_callback' => function () { return current_user_can( 'administrator' ) || current_user_can( 'manager' ); } , 
		'callback' => 'delete_competence',
	));	

	register_rest_route( 'api/v1', '/import_competences_from_last_year', array(
		'methods'  => 'POST',
		'args' => array(
			'email' => array(
				'required' => true,
				'validate_callback' => function($param, $request, $key) {
					return is_email( $param );
				}
			),
			'existImport' => array(
				'validate_callback' => function($param, $request, $key) {
					return is_numeric( $param );
				}
			),
		),		
		'permission_callback' => function () { return current_user_can( 'administrator' ) || current_user_can( 'manager' ); } , 
		'callback' => 'import_competences_from_last_year',
	));	

	register_rest_route( 'api/v1', '/import_json_create_users', array(
		'methods'  => 'POST',
		'permission_callback' => function () { return current_user_can( 'administrator' ); } , 
		'callback' => 'import_json_create_users',
	));	

	register_rest_route( 'api/v1', '/import_json_update_users', array(
		'methods'  => 'POST',
		//'args' => array(),		
		'permission_callback' => function () { return current_user_can( 'administrator' ); } , 
		'callback' => 'import_json_update_users',
	));	
	
	register_rest_route( 'api/v1', '/assessment', array(
		'methods'  => 'GET',
		'args' => array(
			'userID' => array(
				'required' => true,
				'validate_callback' => function($param, $request, $key) {
					return is_numeric( $param );
				}
			),
			'year' => array(
				'required' => false,
				'validate_callback' => function($param, $request, $key) {
					return is_numeric( $param );
				}
			)

		),	
		'permission_callback' => function () { return current_user_can( 'administrator' ) || current_user_can( 'manager' )  || current_user_can( 'subscriber' ) || current_user_can( 'ixion' ); } , 
		'callback' => 'assessment',
	));		

	register_rest_route( 'api/v1', '/insert_update_assessment', array(
		'methods'  => 'POST',
		'permission_callback' => function () { return current_user_can( 'administrator' ) || current_user_can( 'manager' ); } , 
		'callback' => 'insert_update_assessment',
	));	
	
	register_rest_route( 'api/v1', '/language', array(
		'methods'  => 'POST',
		'args' => array(
			'ID_user' => array(
				'required' => true,
				'validate_callback' => function($param, $request, $key) {
					return is_numeric( $param );
				}

			),
			'language' => array(
				'required' =>  true
			)
			
		),
		'permission_callback' => function () { return current_user_can( 'administrator' ) || current_user_can( 'manager' )  || current_user_can( 'subscriber' ) || current_user_can( 'ixion' ); } , 
		'callback' => 'language',
	));	
	
	register_rest_route( 'api/v1', '/insert_competence', array(
		'methods'  => 'POST',
		'args' => array(
			'nameForm' => array(
				'required' => true
			)			
		),
		'permission_callback' => function () { return current_user_can( 'administrator' ); } , 
		'callback' => 'insert_competence',
	));	
	
	register_rest_route( 'api/v1', '/update_competence', array(
		'methods'  => 'POST',
		'args' => array(
			'ID' => array(
				'required' => true,
				'validate_callback' => function($param, $request, $key) {
					return is_numeric( $param );
				}
			),		
		),
		'permission_callback' => function () { return current_user_can( 'administrator' ); } , 
		'callback' => 'update_competence',
	));	

	register_rest_route( 'api/v1', '/delete_competence_item', array(
		'methods'  => 'POST',
		'args' => array(
			'ID' => array(
				'required' => true,
				'validate_callback' => function($param, $request, $key) {
					return is_numeric( $param );
				}
			),		
		),
		'permission_callback' => function () { return current_user_can( 'administrator' ); } , 
		'callback' => 'delete_competence_item',
	));	
	
	register_rest_route( 'api/v1', '/insert_competence_field', array(
		'methods'  => 'POST',
		'args' => array(
			'ID_field' => array(
				'required' => true,
				'validate_callback' => function($param, $request, $key) {
					return is_numeric( $param );
				}
			),
			'field' => array(
				'required' => true
			),		
		),
		'permission_callback' => function () { return current_user_can( 'administrator' ); } , 
		'callback' => 'insert_competence_field',
	));	
	
	register_rest_route( 'api/v1', '/update_competence_field', array(
		'methods'  => 'POST',
		'args' => array(
			'ID' => array(
				'required' => false,
				'validate_callback' => function($param, $request, $key) {
					return is_numeric( $param );
				}
			),
			'ID_field' => array(
				'required' => false,
				'validate_callback' => function($param, $request, $key) {
					return is_numeric( $param );
				}
			),
			'field' => array(
				'required' => true
			),		
		),
		'permission_callback' => function () { return current_user_can( 'administrator' ); } , 
		'callback' => 'update_competence_field',
	));	
	
	register_rest_route( 'api/v1', '/delete_competence_field', array(
		'methods'  => 'POST',
		'args' => array(
			'ID' => array(
				'required' => true,
				'validate_callback' => function($param, $request, $key) {
					return is_numeric( $param );
				}
			),
			'field' => array(
				'required' => true
			),

		),
		'permission_callback' => function () { return current_user_can( 'administrator' ); } , 
		'callback' => 'delete_competence_field',
	));	
	
	##
	register_rest_route( 'api/v1', 'reports', array(
		//'methods'  => 'POST',
		'methods'  => array('GET', 'POST'),
		'permission_callback' => function () { return current_user_can( 'administrator' ) || current_user_can( 'manager' ); } , 
		'callback' => 'reports',
	));		
	register_rest_route( 'api/v1', 'reports_180', array(
		'methods'  => 'POST',
		'permission_callback' => function () { return current_user_can( 'administrator' ) || current_user_can( 'manager' ); } , 
		'callback' => 'reports_180',
	));	


	$manager = new Manager();
	register_rest_route( 'api/v1', 'insert_manager_by_competence', array(
		'methods'  => 'POST',
		'args' => $manager->args(),		
		'permission_callback' => function () { return current_user_can( 'administrator' ) || current_user_can( 'manager' ); } , 
		'callback' => array( $manager, 'insert_manager_by_competence' )
	));	

	register_rest_route( 'api/v1', 'delete_manager_by_competence', array(
		'methods'  => 'POST',
		'args' => $manager->args(),		
		'permission_callback' => function () { return current_user_can( 'administrator' ) || current_user_can( 'manager' ); } , 
		'callback' => array( $manager, 'delete_manager_by_competence' )
	));	


});

function teste1( WP_REST_Request $request ) 
{
	$users = get_users( array( 'search' => $request->get_param("search") ,'fields' => array( 'ID','user_nicename','display_name', 'user_email' ) ) );


	foreach ( $users as $user ) {
		$user->userRoles = get_user_meta( $user->ID );
	}

	// return new WP_Error( 'awesome_no_author', 'Invalid author', array( 'status' => 400 ) );
	return  $users;

}

function users( WP_REST_Request $request ) 
{

	global $wpdb;
	$search = $request->get_param("search");
	$year = $request->offsetExists("year") ? $request->get_param("year") : date( 'Y' );
	//$users = get_users( array( 'meta_query' => array('relation' => 'OR', array('key' => 'user_email', 'value'=> $search, 'compare' => 'LIKE'),array('key' => 'user_nicename', 'value'=> $search, 'compare' => 'LIKE'),array('key' => 'display_name', 'value'=> $search, 'compare' => 'LIKE'), array('key' => 'first_name', 'value'=> $search, 'compare' => 'LIKE'), array('key' => 'last_name', 'value'=> $search, 'compare' => 'LIKE') ) ,'fields' => array( 'ID','user_nicename','display_name', 'user_email' ) ) );
	$currentRoles = wp_get_current_user()->roles;

	$isSubscriber = in_array('subscriber', $currentRoles);
	$isAdministrator = in_array('administrator', $currentRoles);
	$isManager = in_array('manager', $currentRoles);
	$isIxion = in_array('ixion', $currentRoles);

	$args = array(
		'number' => 10,
		'fields' => array( 'ID','user_nicename','display_name', 'user_email' ),
	);

	if ($isManager) { // is manager 
		$args  = array_merge( $args, array(
			'role' => 'subscriber',
			'search'     => '*' . esc_attr( $search ) . '*',
			'search_columns' => array( 'first_name', 'last_name','user_nicename', 'user_login', 'user_email', 'user_nicename', 'display_name'  ),
			'meta_query' => array(
				'relation' => 'OR', 
				array(
					'key'     => 'manage_name',
					'value'   => wp_get_current_user()->user_nicename,
					'compare' => '='
				),			
				array(
					'key'     => 'manage_name',
					'compare' => 'NOT EXISTS'
				),
			)
		));

		$users_r = new WP_User_Query( $args );
		$users = $users_r->get_results();

	}

	else if($isAdministrator){ 
		$args  = array_merge( $args, array(
			'role' => 'subscriber',
			'search'     => '*' . esc_attr( $search ) . '*',
			'search_columns' => array( 'first_name', 'last_name','user_nicename', 'user_login', 'user_email', 'user_nicename', 'display_name'  ),
		));

		$users_r = new WP_User_Query( $args );
		$users = $users_r->get_results();



	}
	else if($isSubscriber || $isIxion){ // retorna somente o proprio colaborador
		$users = [wp_get_current_user()->data];
	}



	$removeValues = array('user_name','nickname','first_name','user_email','last_name','description','rich_editing','syntax_highlighting','comment_shortcuts','admin_color','use_ssl','show_admin_bar_front','locale','wp_capabilities','wp_user_level','dismissed_wp_pointers','show_welcome_panel','wp_dashboard_quick_press_last_post_id','session_tokens','community-events-location');

	foreach ( $users as $user ) {
		$user->userRoles = get_user_meta( $user->ID );
		$user->wp_capabilities = maybe_unserialize($user->userRoles['wp_capabilities'][0]);
		$user->display_name = $user->userRoles['first_name'][0] . " ". $user->userRoles['last_name'][0];
		$user->last_update = $wpdb->get_results( $wpdb->prepare("SELECT MAX(`last_update`) as 'last_update' FROM `{$wpdb->prefix}competences_users` WHERE `ID_user` = $user->ID AND 1;", OBJECT ) )[0]->last_update;

		foreach ($user->userRoles as $key => $element) {
			if (in_array($key, $removeValues)  ) {
				unset($user->userRoles[$key]);
			}
		}

		$queryUserCompetencesThisYear = "SELECT * FROM `{$wpdb->prefix}competences_users`  WHERE `ID_user` = $user->ID AND `date_created` like '". $year ."%' AND 1;";
		$user->competences = $wpdb->get_results( $wpdb->prepare($queryUserCompetencesThisYear, OBJECT ) );

		foreach ($user->competences as $key_c => $competence) {
			$user->competences[$key_c]->slug = str_replace(" ", "_", strtolower($user->competences[$key_c]->competence_name)) ;
		}

	}

	if ( $user ) {
		return  $users;
	}
	return [];

}

function managers( WP_REST_Request $request ) 
{
	global $wpdb;

	$args = array(
		'role' => 'manager',
		'order' => 'ASC',
		'orderby' => 'display_name',
		'exclude' => array(1),
		'search_columns' => array( 'first_name', 'last_name','user_nicename', 'user_login', 'user_email', 'user_nicename', 'display_name'  ),
		'fields' => array( 'ID','user_nicename','display_name', 'user_email' )
	);

	// if ($request->offsetExists("order")) {
	// 	$args["suppress_filters"] =true;
	// 	$args["order"] = $request->get_param("order");
	// }

	// if ($request->offsetExists("orderby")) {
	// 	$args["suppress_filters"]=true;
	// 	$args["orderby"] = $request->get_param("orderby");
	// }

	$users_r = new WP_User_Query( $args );

	$users = $users_r->get_results();

	$args["role"] = 'administrator';
	$admins = new WP_User_Query( $args );
	$users_adms = $admins->get_results();
	$users = array_merge($users_adms, $users);
	ksort($users);

	if ( $users ) {
		return  $users;
	}
	return [];

}

function administrators( WP_REST_Request $request ) 
{
	global $wpdb;

	$args = array(
		'role' => 'administrator',
		'order' => 'ASC',
		'orderby' => 'display_name',
		'exclude' => array(1),
		'search_columns' => array( 'first_name', 'last_name','user_nicename', 'user_login', 'user_email', 'user_nicename', 'display_name'  ),
		'fields' => array( 'ID','user_nicename','display_name', 'user_email' )
	);
	$users_r = new WP_User_Query( $args );

	$users = $users_r->get_results();

	return $users;

}

function get_perfil( WP_REST_Request $request ) 
{
	return wp_get_current_user()->roles;

}

function competences_itens( WP_REST_Request $request ) 
{
	global $wpdb;

	$query = "
	SELECT 
    *,
	I.nameForm,
	(
	select GROUP_CONCAT('\"', field, '\"' SEPARATOR ', ') from {$wpdb->prefix}competences_itens_all_fields 
	WHERE ID_field = I.ID
	) as 'fields'
	, 		(
	select GROUP_CONCAT('\"', ID_manager, '\"' SEPARATOR ', ') from {$wpdb->prefix}competences_itens_managers 
	WHERE ID_competence_item = I.ID
	) as 'ids_managers_authorized'
	FROM 
	{$wpdb->prefix}competences_itens as I";

	if ($request->offsetExists("idManager")) {
		$idManager = $request->get_param('idManager');
		$query .= " WHERE I.ID IN ( select ID_competence_item from {$wpdb->prefix}competences_itens_managers where ID_manager = $idManager );";
	}

	else {
		$query .= " WHERE 1;";
	}
	

	$json = $wpdb->get_results( $wpdb->prepare( $query	, OBJECT));

	// print_r($wpdb->last_query);

	foreach ($json as $key => $value) {
		$value->nameForm = __($value->nameForm,"competencesmapping");
		$value->fields = json_decode(replace_caracteres("[".$value->fields."]"), true);
		$value->ids_managers_authorized = json_decode(replace_caracteres("[".$value->ids_managers_authorized."]"), true);
	}



	return $json;

}

function replace_caracteres($str='')
{

   $str = str_replace(", ]", "]", $str);

   return str_replace(",]", "]", $str);
}

function create_user_and_competence( WP_REST_Request $request ) 
{

	global $wpdb;

	if ( $request->offsetExists("ID") && $request->get_param('ID') != email_exists($user_email) ) { // teve update de email
		$user_id = $request->get_param("ID");
	}

	else {
		if (!$request->offsetExists("email")) {
			return new WP_Error( 'no_email', 'Invalid parameter(s): email', array( 'status' => 400 ) );
		}

		$user_name = explode("@", $request->get_param('email'))[0];
		$user_email = $request->get_param('email');

		$user_id = username_exists( $user_name );



		if ($user_id === false) {
			$user_id = email_exists( $user_email );
		}


		if ( !$user_id and email_exists($user_email) == false ) {
			$random_password = wp_generate_password( $length=12, $include_standard_special_chars=false );
			$user_id = wp_create_user( $user_name, $random_password, $user_email );
		} 
	}


	wp_update_user( array( 'ID' => $user_id, 'first_name' => $request->get_param('user_name') ) );

	$profile = $request->get_param('profile');

	foreach ($profile as $key => $elem) {
		if (is_array($elem) ) {
			$array_to_string = "";
			foreach ($elem as $key1 => $elem1) {

				if ($key === 'experience') {
					$array_to_string .= $elem1['segment_experience'] . " (". $elem1['experience'] ."), ";
				}

			}
			update_user_meta( $user_id, $key, $array_to_string );
		}

		else {
			update_user_meta( $user_id, $key, $elem );

		}
	}

	$competences = $request->get_param('competences');

	foreach ($competences as $key => $competence) {
		$data = array(
			'ID_user'=> $user_id,
			'competence_name'=> $competence['competence_name'],
			'competence_selected'=> $competence['competence_selected'],
			'reached'=> $competence['reached'],
			'expected'=> $competence['expected'],
			'priority'=> $competence['priority'],
			'development'=> $competence['development'],
			'development_suggestion'=> $competence['development_suggestion'],
			'seniority_level'=> $competence['seniority_level'],
			'date_created'=> date( 'Y-m-d H:i:s'),
			'last_update'=>date( 'Y-m-d H:i:s' )
		);

		if (array_key_exists('ID',$competence)) { // update
			unset($data["ID_user"], $data["date_created"]);
			
			$result = $wpdb->update( "{$wpdb->prefix}competences_users", $data, array( 'ID' => $competence["ID"], 'ID_user' => $user_id ) );
		}

		else { // insert
			$result = $wpdb->insert("{$wpdb->prefix}competences_users",$data);

		}

	}

	if (false === $result) {
		return new WP_Error( 'no_insert_or_update', 'Invalid fields ', array( 'status' => 400 ) );
	}

	return array('error' => false, 'data' => "Sucess!" );
}

function create_delete_manager( WP_REST_Request $request ) 
{

	if ( $request->offsetExists("ID") && $request->get_param("action") == "delete"  ) { 
		require_once ABSPATH . 'wp-admin/includes/user.php';

		$sucess = wp_delete_user(intval($request->get_param("ID")), 1);

		return array('error' => !$sucess, 'data' => 'Deleted!' );
	}

	else if ( $request->offsetExists("login") && $request->get_param("action") == "create"  ) { 

		$user_name = $request->get_param('login');

		$user_id = username_exists( $user_name );

		if (!$request->offsetExists("password")) {
			return new WP_Error( 'password', 'Password required', array( 'status' => 400 ) );
		}

		if ( !$user_id ) {
			//$random_password = wp_generate_password( $length=12, $include_standard_special_chars=false );
			$random_password = $request->get_param('password');
			$user_id = wp_create_user( $user_name, $random_password );

		} 

		wp_update_user( array( 'ID' => $user_id, 'display_name' => $request->get_param('name') ) );

		$u = new WP_User( $user_id );
		$u->remove_role( 'subscriber' );
		$u->add_role( 'manager' );

		return array('error' => false, 'data' => $user_id );
	}

	
}
function create_delete_administrator( WP_REST_Request $request ) 
{

	if ( $request->offsetExists("ID") && $request->get_param("action") == "delete"  ) { 
		require_once ABSPATH . 'wp-admin/includes/user.php';

		$sucess = wp_delete_user(intval($request->get_param("ID")), 1);

		return array('error' => !$sucess, 'data' => 'Deleted!' );
	}

	else if ( $request->offsetExists("login") && $request->get_param("action") == "create"  ) { 

		$user_name = $request->get_param('login');

		$user_id = username_exists( $user_name );

		if (!$request->offsetExists("password")) {
			return new WP_Error( 'password', 'Password required', array( 'status' => 400 ) );
		}

		if ( !$user_id ) {
			//$random_password = wp_generate_password( $length=12, $include_standard_special_chars=false );
			$random_password = $request->get_param('password');
			$user_id = wp_create_user( $user_name, $random_password );

		} 

		wp_update_user( array( 'ID' => $user_id, 'display_name' => $request->get_param('name') ) );

		$u = new WP_User( $user_id );
		$u->remove_role( 'subscriber' );
		$u->add_role( 'administrator' );

		return array('error' => false, 'data' => $user_id );
	}

	
}

function delete_competence( WP_REST_Request $request ) {
	global $wpdb;

	$result = $wpdb->delete( "{$wpdb->prefix}competences_users",  array('ID' => $request->get_param('ID') ) );

	if (false === $result) {
		return new WP_Error( 'deleted_compenteces', 'Deleted error ', array( 'status' => 400 ) );
	}

	return array('error' => false, 'data' => 'Deleted! '  );
}

function import_competences_from_last_year( WP_REST_Request $request ) {
	global $wpdb;

	$userID = email_exists($request->get_param("email"));
	$last_year = date( 'Y' ) - 1;

	$queryUserCompetencesLastYear = "SELECT * FROM `{$wpdb->prefix}competences_users`  WHERE `ID_user` = $userID AND `date_created` like '". $last_year ."%' AND 1;";
	$competences = $wpdb->get_results( $wpdb->prepare($queryUserCompetencesLastYear, OBJECT ) );

	if ($request->offsetExists("existImport")) { 
		return $wpdb->num_rows;
	}

	foreach ($competences as $key_c => $competence) {
		$competences[$key_c]->ID_temp = $competences[$key_c]->ID;
		unset( $competences[$key_c]->ID, $competences[$key_c]->date_created, $competences[$key_c]->last_update );
		$competences[$key_c]->slug = str_replace(" ", "_", strtolower($competences[$key_c]->competence_name));
	}


	return $competences;
}

function import_json_create_users( WP_REST_Request $request ) 
{

	global $wpdb;

	if (is_array($request->get_param('data'))) {
		
		foreach ($request->get_param("data") as $key => $request1) {

			$user_name = $request1["nickname"];
			$user_email = $request1["email"];

			$user_id = username_exists( $user_name );


			if ($user_id === false) {
				$user_id = email_exists( $user_email );
			}


			if ( !$user_id and email_exists($user_email) == false ) {
				$random_password = $request1["senha"];
				$user_id = wp_create_user( $user_name, $random_password, $user_email );
			} 


			wp_update_user( array( 'ID' => $user_id, 'first_name' => $request1["user_name"], 'display_name' => $request1["user_name"] ) );


			$u = new WP_User( $user_id );
			$u->remove_role( 'subscriber' );
			$u->add_role( $request1["perfil"] );

			$profile = $request1;

			foreach ($profile as $key => $elem) {
				if (is_array($elem) ) {
					$array_to_string = "";
					foreach ($elem as $key1 => $elem1) {

						if ($key === 'experience') {
							$array_to_string .= $elem1['segment_experience'] . " (". $elem1['experience'] ."), ";
						}

					}
					update_user_meta( $user_id, $key, $array_to_string );
				}

				else {

					if (!in_array($key, array('senha', 'perfil'))) {
						update_user_meta( $user_id, $key, $elem );
					}


				}
			}

		}

	}

	else {
		return $request->get_param("body");
	}




	return array('error' => false, 'data' => $user_name );
}
function import_json_update_users( WP_REST_Request $request ) 
{

	global $wpdb;

	if (is_array($request->get_param('data'))) {
		
		foreach ($request->get_param("data") as $key => $request1) {

			$user_name = $request1["nickname"];

			$user_id = username_exists( $user_name );


			if ($user_id) {

				$profile = $request1;

				foreach ($profile as $key => $elem) {
					if (is_array($elem) ) {
						$array_to_string = "";
						foreach ($elem as $key1 => $elem1) {

							if ($key === 'experience') {
								$array_to_string .= $elem1['segment_experience'] . " (". $elem1['experience'] ."), ";
							}

						}
						update_user_meta( $user_id, $key, $array_to_string );
					}

					else {

						if (!in_array($key, array('senha', 'perfil'))) {
							update_user_meta( $user_id, $key, $elem );
						}


					}
				}

			}

		}

	}

	else {
		return $request->get_param("body");
	}




	return array('error' => false, 'data' => $user_name );
}

function assessment( WP_REST_Request $request ) 
{

	global $wpdb;
	$userID = $request->get_param("userID");
	$year = $request->offsetExists("year") ? $request->get_param("year") : date( 'Y' );

	if ($request->get_method() == "GET") {
		$query = "SELECT * FROM `{$wpdb->prefix}competences_180_assessment`  WHERE `ID_user` = $userID AND `date_created` like '". $year ."%' AND 1;";

		return $wpdb->get_results( $wpdb->prepare($query, OBJECT ) );
	}

}

function insert_update_assessment( WP_REST_Request $request )
{

	global $wpdb;

	$data = array(
		'Outside - in'=> $request->offsetExists("Outside - in") ? $request->get_param("Outside - in") : 0,
		'Accountability'=> $request->offsetExists("Accountability") ? $request->get_param("Accountability"): 0,
		'Empowerment'=> $request->offsetExists("Empowerment") ? $request->get_param("Empowerment"): 0,
		'Teamwork'=> $request->offsetExists("Teamwork") ? $request->get_param("Teamwork") : 0,
		'Openness'=> $request->offsetExists("Openness") ? $request->get_param("Openness") : 0,
		'High Ethics'=> $request->offsetExists("High Ethics") ? $request->get_param("High Ethics") : 0
	);

	if ($request->offsetExists("ID")) {
		$result = $wpdb->update( "{$wpdb->prefix}competences_180_assessment", $data, array( 'ID' => $request->get_param("ID")) );
	}

	else {
		$data["ID_user"] = $request->get_param("ID_user");

		$result = $wpdb->insert( "{$wpdb->prefix}competences_180_assessment", $data );
	}

	if (false === $result) {
		//return $wpdb->print_error();
		return new WP_Error( 'no_insert_or_update', $wpdb->last_error, array( 'status' => 400 ) );
	}

	return array('error' => false, 'data' => $result );
}

function language( WP_REST_Request $request )
{
	//return  get_user_meta( get_current_user_id(), 'locale', true );

	//return __($request->get_param("language"),"competencesmapping");

	$r = update_user_meta( $request->get_param("ID_user"), "locale", $request->get_param("language"), '' );

	return array('error' => !boolval( $r ), 'data' => $request->get_param("language") );

}

function insert_competence( WP_REST_Request $request )
{

	global $wpdb;

	$data = array(
		'nameForm'=> $request->get_param("nameForm"),
		'slug'=> str_replace(" ", "_", strtolower($request->get_param("nameForm"))),
	);


	$result = $wpdb->insert("{$wpdb->prefix}competences_itens", $data );

	if (false === $result) {
		return new WP_Error( 'no_insert_or_update', $wpdb->last_error, array( 'status' => 400 ) );
	}

	return array('error' => false, 'data' => $result );

}

function update_competence( WP_REST_Request $request )
{

	global $wpdb;

	$data = array(
		'nameForm'=> $request->get_param("nameForm"),
		'slug'=> str_replace(" ", "_", strtolower($request->get_param("nameForm"))),
	);


	if ($request->offsetExists("ID")) {
		$result = $wpdb->update("{$wpdb->prefix}competences_itens", $data, array( 'ID' => $request->get_param("ID")) );
	}


	if (false === $result) {
		return new WP_Error( 'no_insert_or_update', $wpdb->last_error, array( 'status' => 400 ) );
	}

	return array('error' => false, 'data' => $result );

}
function delete_competence_item( WP_REST_Request $request )
{

	global $wpdb;

	$result = $wpdb->delete("{$wpdb->prefix}competences_itens",  array( 'ID' => $request->get_param("ID")) );

	if (false === $result) {
		return new WP_Error( 'no_insert_or_update', $wpdb->last_error, array( 'status' => 400 ) );
	}

	return array('error' => false, 'data' => $result );
}

function insert_competence_field( WP_REST_Request $request )
{

	global $wpdb;

	$data = array(
		'ID_field'=> $request->get_param("ID_field"),
		'field'=> $request->get_param("field"),
	);


	$result = $wpdb->insert( "{$wpdb->prefix}competences_itens_all_fields", $data );

	if (false === $result) {
		return new WP_Error( 'no_insert_or_update', $wpdb->last_error, array( 'status' => 400 ) );
	}

	return array('error' => false, 'data' => $result );

}

function update_competence_field( WP_REST_Request $request )
{

	global $wpdb;

	$data = array(
		'ID_field'=> $request->get_param("ID_field"),
		'field'=> $request->get_param("field"),
	);


	$result = $wpdb->update( "{$wpdb->prefix}competences_itens_all_fields", $data, array( 'ID_field' => $request->get_param("ID_field"), 'field' => $request->get_param("last_field")) );

	if (false === $result) {
		return new WP_Error( 'no_insert_or_update', $wpdb->last_error, array( 'status' => 400 ) );
	}

	return array('error' => false, 'data' => $result );

}

function delete_competence_field( WP_REST_Request $request )
{

	global $wpdb;


	$result = $wpdb->delete( "{$wpdb->prefix}competences_itens_all_fields",  array( 'ID_field' => $request->get_param("ID"), 'field' => $request->get_param("field") ) );

	if (false === $result) {
		return new WP_Error( 'no_insert_or_update', $wpdb->last_error, array( 'status' => 400 ) );
	}

	return array('error' => false, 'data' => $result );

}
function reports( WP_REST_Request $request )
{

	global $wpdb;

	$query = "
	SELECT 
	U.user_login  as '".__('Chapa', 'competencesmapping')."'
	, U.display_name as '".__('Name', 'competencesmapping')."'
	, USER_META3.meta_value as '".__('Email', 'competencesmapping')."'
	, USER_META4.meta_value as '".__('Country', 'competencesmapping')."'
	, USER_META5.meta_value as '".__('City', 'competencesmapping')."'
	, USER_META6.meta_value as '".__('State or Province', 'competencesmapping')."'
	, C.competence_name as '".__('Competence name', 'competencesmapping')."'
	, C.competence_selected as '".__('Competence selected', 'competencesmapping')."'
	, C.reached as '".__('Reached', 'competencesmapping')."'
	, C.expected as '".__('Expected', 'competencesmapping')."'
	, C.priority as '".__('Priority', 'competencesmapping')."'
	, C.development	as '".__('Development', 'competencesmapping')."'
	, C.development_suggestion	as '".__('Development Suggestion', 'competencesmapping')."'
	, C.seniority_level	as '".__('Seniority Level', 'competencesmapping')."'
	, USER_META.meta_value as '".__('Experiences', 'competencesmapping')."'
	, ( select UM.display_name from {$wpdb->prefix}users as UM where UM.user_login = USER_META1.meta_value  ) as '".__('Manage Name', 'competencesmapping')."'
	, USER_META2.meta_value as '".__('Main Global Customer Contact', 'competencesmapping')."'
	, C.date_created as '".__('Date Created', 'competencesmapping')."'
	, C.last_update as '".__('Last Update', 'competencesmapping'). "'


	FROM `{$wpdb->prefix}competences_users` as C
	INNER JOIN `{$wpdb->prefix}users` as U
	ON U.ID = C.ID_user
	INNER JOIN `{$wpdb->prefix}usermeta` as USER_META
	ON USER_META.user_id = C.ID_user and USER_META.meta_key = 'experience'
	INNER JOIN `{$wpdb->prefix}usermeta` as USER_META1
	ON USER_META1.user_id = C.ID_user and USER_META1.meta_key = 'manage_name'
	INNER JOIN `{$wpdb->prefix}usermeta` as USER_META2
	ON USER_META2.user_id = C.ID_user and USER_META2.meta_key = 'main_global_customer_contact'
	INNER JOIN `{$wpdb->prefix}usermeta` as USER_META3
	ON USER_META3.user_id = C.ID_user and USER_META3.meta_key = 'email'
	INNER JOIN `{$wpdb->prefix}usermeta` as USER_META4
	ON USER_META4.user_id = C.ID_user and USER_META4.meta_key = 'country'
	INNER JOIN `{$wpdb->prefix}usermeta` as USER_META5
	ON USER_META5.user_id = C.ID_user and USER_META5.meta_key = 'city'
	INNER JOIN `{$wpdb->prefix}usermeta` as USER_META6
	ON USER_META6.user_id = C.ID_user and USER_META6.meta_key = 'state_or_province'



	WHERE 1";

	if ($request->offsetExists("competence_ID") && $request->get_param('competence_ID') != "") {

		$query2 = "SELECT `nameForm` FROM `{$wpdb->prefix}competences_itens` WHERE `ID` = ".$request->get_param('competence_ID'); // get nome da competencia
		$competence_name = $wpdb->get_results( $wpdb->prepare($query2, OBJECT ) );

		if (false !== $competence_name) {
			$s = esc_sql('%'.$competence_name[0]->nameForm.'%');
			$query .= " and `competence_name` like '$s'";
		}
	}

	if ($request->offsetExists("name") && $request->get_param("name") != "") {
		$s = esc_sql('%'.$request->get_param("name").'%');
		$query .= " and U.display_name like '$s'";
	}
	if ($request->offsetExists("manage_name") && $request->get_param("manage_name") != "") {
		$s = esc_sql('%'.$request->get_param("manage_name").'%');
		$query .= " and USER_META1.meta_value IN(select UM2.user_login from {$wpdb->prefix}users as UM2 where UM2.display_name like '$s' ) ";
	}

	if ($request->offsetExists("main_global_customer_contact") && $request->get_param("main_global_customer_contact") != "") {
		$s = esc_sql('%'.$request->get_param("main_global_customer_contact").'%');
		$query .= " and USER_META2.meta_value like '$s'";
	}

	if ($request->offsetExists("competence_selected") && $request->get_param("competence_selected") != "") {
		$s = esc_sql('%'.$request->get_param("competence_selected").'%');
		$query .= " and competence_selected like '$s'";
	}

	if ($request->offsetExists("experience") && $request->get_param("experience") != "") {
		$s = esc_sql('%'.$request->get_param("experience").'%');
		$query .= " and USER_META.meta_value like '$s'";
	}

	if ($request->offsetExists("segment_experience") && $request->get_param("segment_experience") != "") {
		$s = esc_sql('%'.$request->get_param("segment_experience").'%');
		$query .= " and USER_META.meta_value like '$s'";
	}


	if ($request->offsetExists("reached") && $request->get_param("reached") != "") {
		$query .= " and `reached` = '". $request->get_param("reached") ."'";
	}

	if ($request->offsetExists("expected") && $request->get_param("expected") != "") {
		$query .= " and `expected` = '". $request->get_param("expected") ."'";
	}

	if ($request->offsetExists("priority") && $request->get_param("priority") != "") {
		$query .= " and `priority` = '". $request->get_param("priority") ."'";
	}

	if ($request->offsetExists("development") && $request->get_param("development") != "") {
		$s = esc_sql('%'.$request->get_param("development").'%');
		$query .= " and `development` like '$s'";
	}

	if ($request->offsetExists("seniority_level") && $request->get_param("seniority_level") != "") {
		$s = esc_sql('%'.$request->get_param("seniority_level").'%');
		$query .= " and `seniority_level` like '$s'";
	}

	if ($request->offsetExists("year")) {
		$s = esc_sql('%'.$request->get_param("year").'%');
		$query .= " and C.`date_created` like '$s'";
	}


	$query .= " ORDER BY C.`ID_user` DESC";
##

	$result = $wpdb->get_results( $wpdb->prepare($query, OBJECT ) );

	// print_r($wpdb->last_query);
	// return ;

	if ($request->offsetExists("download")) { // download excel


		$planilha = new ExportToXLS([],[]);

		$response = new WP_REST_Response($planilha->download(),200);

		$response->header("Expires", "Mon, 26 Jul 1997 05:00:00 GMT");
		$response->header("Last-Modified",  gmdate("D,d M YH:i:s") . " GMT");
		$response->header("Cache-Control", "max-age=0");
		$response->header("Pragma","public");
		//$response->header("Content-type","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		$response->header("Content-type","application/vnd.ms-excel");
		//$response->header("Content-type","application/x-msexcel");
		//$response->header("Content-type","force-download");
		$response->header("x-filename","{$planilha->arquivo}");
		//$response->header("Content-Transfer-Encoding","binary");
		$response->header("Content-Disposition", "attachment; filename=\"{$planilha->arquivo}\"" );
		$response->header("Content-Description", "PHP Generated Data" );

		//$response->set_data( $planilha->download() );
		return $response;
		die();    

	}





	if (false === $result) {
		return new WP_Error( 'no_insert_or_update', $wpdb->last_error, array( 'status' => 400 ) );
	}

	return array('error' => false, 'data' => $result );

}

function reports_180( WP_REST_Request $request ){

	global $wpdb;

	$query = "SELECT 
	U.user_login as Login
	, U.display_name as Name
	, USER_META3.meta_value as 'Email'
	, ( select UM.display_name from {$wpdb->prefix}users as UM where UM.user_login = USER_META1.meta_value  ) as 'Manager Name'
	, USER_META2.meta_value as 'Main Global Customer contact'

	, USER_META4_1.meta_value as '".__('Country', 'competencesmapping')."'
	, USER_META5_1.meta_value as '".__('City', 'competencesmapping')."'
	, USER_META6_1.meta_value as '".__('State or Province', 'competencesmapping')."'


	, `Outside - in`
	, `Accountability`
	, `Empowerment`
	, `Teamwork`
	, `Openness`
	, `High Ethics`
	, `date_created` as 'Date Created'

	FROM {$wpdb->prefix}competences_180_assessment as A
	INNER JOIN {$wpdb->prefix}users as U
	ON U.ID = A.ID_user 
	INNER JOIN `{$wpdb->prefix}usermeta` as USER_META1
	ON USER_META1.user_id = A.ID_user and USER_META1.meta_key = 'manage_name'
	INNER JOIN `{$wpdb->prefix}usermeta` as USER_META2
	ON USER_META2.user_id = A.ID_user and USER_META2.meta_key = 'main_global_customer_contact'
	INNER JOIN `{$wpdb->prefix}usermeta` as USER_META3
	ON USER_META3.user_id = A.ID_user and USER_META3.meta_key = 'email'

	INNER JOIN `{$wpdb->prefix}usermeta` as USER_META4_1
	ON USER_META4_1.user_id = A.ID_user and USER_META4_1.meta_key = 'country'
	INNER JOIN `{$wpdb->prefix}usermeta` as USER_META5_1
	ON USER_META5_1.user_id = A.ID_user and USER_META5_1.meta_key = 'city'
	INNER JOIN `{$wpdb->prefix}usermeta` as USER_META6_1
	ON USER_META6_1.user_id = A.ID_user and USER_META6_1.meta_key = 'state_or_province'

	where 1
	";

	if ($request->offsetExists("name") && $request->get_param("name") != "") {
		$s = esc_sql('%'.$request->get_param("name").'%');
		$query .= " and U.display_name like '$s'";
	}
	if ($request->offsetExists("manage_name") && $request->get_param("manage_name") != "") {
		$s = esc_sql('%'.$request->get_param("manage_name").'%');
		$query .= " and USER_META1.meta_value IN(select UM2.user_login from {$wpdb->prefix}users as UM2 where UM2.display_name like '$s' ) ";
	}

	if ($request->offsetExists("main_global_customer_contact") && $request->get_param("main_global_customer_contact") != "") {
		$s = esc_sql('%'.$request->get_param("main_global_customer_contact").'%');
		$query .= " and USER_META2.meta_value like '$s'";
	}

	$result = $wpdb->get_results( $wpdb->prepare($query, OBJECT ) );

	if (false === $result) {
		return new WP_Error( 'no_report_180', $wpdb->last_error, array( 'status' => 400 ) );
	}

	return array('error' => false, 'data' => $result );

}