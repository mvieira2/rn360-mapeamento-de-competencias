
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Competence mapping</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script src="<?php echo get_template_directory_uri(); ?>/includes/angular/js/angular.min.js"></script>
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/includes/bootstrap/css/bootstrap.min.css">
	<script src="<?php echo get_template_directory_uri(); ?>/includes/jquery/js/jquery.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/includes/notify/bootstrap-notify.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/includes/bootstrap/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/includes/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/style.css?v=<?php echo get_rand(); ?>">
	<script src="<?php echo get_template_directory_uri(); ?>/includes/jquery/js/jquery-ui.min.js"></script>
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/includes/jquery/css/jquery-ui.min.css">
	<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/img/favicon.ico">
	<?php //wp_head(); ?>

	<script type="text/javascript">
		var GLOBALS = {
			alerts : {
				msg1 : "<?php _e('There are existing fields of this competence. Delete them first.', 'competencesmapping'); ?>"
			}
		}

	</script>

</head>
<body ng-app="myApp"  class="ng-cloak">
	
	<header ng-controller="Header" >
		<?php if( !is_home() && !is_front_page() ):?>
		<div class="user-info">
			<div class="center">

				<nav class="navbar1">
					<div class="container-fluid">
						<ul class="nav navbar-nav navbar-right">
							<li><?php _e('Hi', 'competencesmapping'); ?>, <b id="userName"><?php _e('User', 'competencesmapping'); ?></b> | </li>
							
							<li ng-show="perfilName1.length && !perfilEnable1('subscriber')" class="ng-hide  "><a href="<?php echo home_url('administration'); ?>">
								<span class="desktop desktopInline"><?php _e('administration', 'competencesmapping'); ?></span> <span class="mobile mobileInline">ADM</span>  |</a> </li>
							
							<li class="dropdown desktop hide">
								<a class="dropdown-toggle" data-toggle="dropdown" href="#">
									<?php
									if (isset($_GET["lang"]) && $_GET["lang"] == "pt_BR") {
										_e('Portuguese', 'competencesmapping');
									}
									else if (isset($_GET["lang"]) && $_GET["lang"] == "es_ES") {
										_e('Spanish', 'competencesmapping');
									}
									else {
										_e('English', 'competencesmapping');
									}
									;?> 
									<span class="caret"></span>
									| 
								</a>
								<ul class="dropdown-menu">
									<li><a href="?lang=en"><?php _e('English', 'competencesmapping'); ?></a></li>
									<li><a href="?lang=es_ES"><?php _e('Spanish', 'competencesmapping'); ?></a></li>
									<li><a href="?lang=pt_BR"><?php _e('Portuguese', 'competencesmapping'); ?></a></li>
								</ul>
							</li>
							<li class=""><a href="<?php echo home_url(); ?>"> <?php _e('logout', 'competencesmapping'); ?></a></li>
						</ul>
					</div>
				</nav>
			</div>
		</div>
	<?php endif;?>
	<div id="topheader" class="">
		<div id="site-header" class="">
			<a href="<?php echo home_url('home');?>" class="">
				<img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="SKF logo" class="supergraph">
			</a>

		</div>

		<div class="supergraph-border"></div>
		<div id="loader">
			<i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i>
		</div>
	</div>
</header>


