<?php get_header(); ?>

<h1 class="text-center" style="margin-bottom: 30px;">ADM Competence mapping </h1>
<main ng-app="myApp" ng-controller="Admin" class="ng-cloak">
	<div class="container">

		<div class="center">
			<a href="<?php echo home_url('home');?>"><i class="fa fa-angle-double-left"></i> <?php _e('Back', 'competencesmapping'); ?></a>
			<br><br>
		</div>

		<div class="row">
			<div class="col-sm-12">
				<div class="center">
					<ul class="nav nav-tabs" >

						<li ng-class="{'active' : perfilEnable('administrator') || perfilEnable('manager')}">
							<a data-toggle="tab" href="#menu-reports" > <?php _e('Reports', 'competencesmapping'); ?></a>
						</li>
						<li ng-show="perfilEnable('administrator')">
							<a data-toggle="tab" href="#menu-1" > <?php _e('Competences', 'competencesmapping'); ?></a>
						</li>						
						<li ng-show="perfilEnable('administrator')">
							<a data-toggle="tab" href="#menu-1-1" > <?php _e('Managers', 'competencesmapping'); ?></a>
						</li>					
						<li ng-show="perfilEnable('administrator')">
							<a data-toggle="tab" href="#menu-1-2" > <?php _e('Administrators', 'competencesmapping'); ?></a>
						</li>


					</ul>
				</div>
				<br>
			</div>
			<div class="col-sm-12">

				
				<div class="center">
					<div class="tab-content">
						<div id="menu-reports" class="tab-pane fade in active" >

							<div class="row">
								<div class="col-sm-12">

									<ul class="nav nav-pills">
										<li class="active">
											<a class="btn btn-default" data-toggle="tab" href="#box1-filter"> <?php _e('Competences', 'competencesmapping'); ?></a>
										</li>
										<li>
											<a class="btn btn-default" data-toggle="tab" href="#box2-filter"> <?php _e('180º Assessment', 'competencesmapping'); ?></a>
										</li>
									</ul>
									<br>
									<div class="tab-content">
										<div id="box1-filter" class="tab-pane fade in active">
											<div class=" filter" >
												<form novalidate>
													<b> <?php _e('Filter', 'competencesmapping'); ?></b>
													<br>
													<br>
													<div class="row">
														<div class="col-sm-4">
															<div class="form-group">
																<input type="text" class="form-control" ng-model="filter.name" placeholder="<?php _e('Name', 'competencesmapping'); ?>" title="<?php _e('Name', 'competencesmapping'); ?>">
															</div>
														</div>	

														<div class="col-sm-4">
															<div class="form-group">
																<input type="text" class="form-control" ng-model="filter.manage_name" placeholder="<?php _e('Manager Name', 'competencesmapping'); ?>" title="<?php _e('Manager Name', 'competencesmapping'); ?>">
															</div>
														</div>	

														<div class="col-sm-4">
															<div class="form-group">
																<input type="text" class="form-control" ng-model="filter.main_global_customer_contact" placeholder="<?php _e('Main Global Customer contact', 'competencesmapping'); ?>" title="<?php _e('Main Global Customer contact', 'competencesmapping'); ?>">
															</div>
														</div>	

														<div class="col-sm-6">
															<div class="form-group">
																<select class="form-control" ng-model="filter.competence_ID" ng-change="filter_setCompetence(filter.competence_ID)" ng-options="model.ID as model.nameForm for model in competences">
																	<option value=""> <?php _e('Competence Name', 'competencesmapping'); ?></option>
																</select>
															</div>
														</div>

														<div class="col-sm-6">
															<div class="form-group">
																<select class="form-control" ng-model="filter.competence_selected" ng-options="model as model for model in competences_selecteds" ng-disabled="!filter.competence_ID">
																	<option value=""><?php _e('Competence selected', 'competencesmapping'); ?></option>
																</select>
															</div>
														</div>	
														<div class="col-sm-6">
															<div class="form-group">
																<select class="form-control" ng-model="filter.segment_experience">
																	
																	<option value=""><?php _e('Segment experience', 'competencesmapping'); ?>:</option>
																	<option value="<?php _e('Oil & Gas', 'competencesmapping'); ?>"><?php _e('Oil & Gas', 'competencesmapping'); ?></option>
																	<option value="<?php _e('Pulp & Paper', 'competencesmapping'); ?>"><?php _e('Pulp & Paper', 'competencesmapping'); ?></option>
																	<option value="<?php _e('Metal Work', 'competencesmapping'); ?>"><?php _e('Metal Work', 'competencesmapping'); ?></option>
																	<option value="<?php _e('Food & Beverage', 'competencesmapping'); ?>"><?php _e('Food & Beverage', 'competencesmapping'); ?></option>
																	<option value="<?php _e('Mining', 'competencesmapping'); ?>"><?php _e('Mining', 'competencesmapping'); ?></option>
																	<option value="<?php _e('Marine', 'competencesmapping'); ?>"><?php _e('Marine', 'competencesmapping'); ?></option>
																	<option value="<?php _e('Cement', 'competencesmapping'); ?>"><?php _e('Cement', 'competencesmapping'); ?></option>
																	<option value="<?php _e('Chemical', 'competencesmapping'); ?>"><?php _e('Chemical', 'competencesmapping'); ?></option>
																	<option value="<?php _e('Wind power', 'competencesmapping'); ?>"><?php _e('Wind power', 'competencesmapping'); ?></option>
																	<option value="<?php _e('Agricultural', 'competencesmapping'); ?>"><?php _e('Agricultural', 'competencesmapping'); ?></option>
																	<option value="<?php _e('Sugar & Ethanol', 'competencesmapping'); ?>"><?php _e('Sugar & Ethanol', 'competencesmapping'); ?></option>

																</select>
															</div>
														</div>	
														<div class="col-sm-6">
															<div class="form-group">
																<select class="form-control" ng-model="filter.experience">
																	<option value=""><?php _e('Experience', 'competencesmapping'); ?></option>
																	<option value="<?php _e('Junior', 'competencesmapping'); ?>"><?php _e('Junior (1 to 3 years)', 'competencesmapping'); ?></option>
																	<option value="<?php _e('Full', 'competencesmapping'); ?>"><?php _e('Full (4 to 6 years)', 'competencesmapping'); ?></option>
																	<option value="<?php _e('Senior', 'competencesmapping'); ?>"><?php _e('Senior (6 to 10 years)', 'competencesmapping'); ?></option>
																	<option value="<?php _e('Master', 'competencesmapping'); ?>"><?php _e('Master (more 10 years)', 'competencesmapping'); ?></option>
																</select>
															</div>
														</div>	
														<div class="col-sm-2">
															<div class="form-group">
																<select class="form-control" ng-model="filter.expected">
																	<option value=""><?php _e('Expected', 'competencesmapping'); ?></option>
																	<option value="0">0</option>
																	<option value="1">1</option>
																	<option value="2">2</option>
																	<option value="3">3</option>
																	<option value="4">4</option>
																	<option value="5">5</option>
																</select>
															</div>
														</div>
														<div class="col-sm-2">
															<div class="form-group">
																<select class="form-control" ng-model="filter.reached">
																	<option value=""><?php _e('Reached', 'competencesmapping'); ?></option>
																	<option value="0">0</option>
																	<option value="1">1</option>
																	<option value="2">2</option>
																	<option value="3">3</option>
																	<option value="4">4</option>
																	<option value="5">5</option>
																</select>
															</div>
														</div>


														<div class="col-sm-2">
															<div class="form-group">
																<select class="form-control" ng-model="filter.priority">
																	<option value=""><?php _e('Priority', 'competencesmapping'); ?></option>
																	<option value="0">0</option>
																	<option value="1">1</option>
																	<option value="2">2</option>
																	<option value="3">3</option>
																	<option value="4">4</option>
																	<option value="5">5</option>
																</select>
															</div>
														</div>
														<div class="col-sm-2">
															<div class="form-group">
																<select class="form-control" ng-model="filter.development">
																	<option value=""><?php _e('Suggestion of Action', 'competencesmapping'); ?>:</option>
																	<option value="<?php _e('Books / Journal', 'competencesmapping'); ?>"><?php _e('Books / Journal', 'competencesmapping'); ?></option>
																	<option value="<?php _e('Coaching', 'competencesmapping'); ?>"><?php _e('Coaching', 'competencesmapping'); ?></option>
																	<option value="<?php _e('Conference', 'competencesmapping'); ?>"><?php _e('Conference', 'competencesmapping'); ?></option>
																	<option value="<?php _e('Customer visit', 'competencesmapping'); ?>"><?php _e('Customer visit', 'competencesmapping'); ?></option>
																	<option value="<?php _e('Exhibition', 'competencesmapping'); ?>"><?php _e('Exhibition', 'competencesmapping'); ?></option>
																	<option value="<?php _e('Job rotation', 'competencesmapping'); ?>"><?php _e('Job rotation', 'competencesmapping'); ?></option>
																	<option value="<?php _e('Learning on the job', 'competencesmapping'); ?>"><?php _e('Learning on the job', 'competencesmapping'); ?></option>
																	<option value="<?php _e('Mentoring', 'competencesmapping'); ?>"><?php _e('Mentoring', 'competencesmapping'); ?></option>
																	<option value="<?php _e('Networking', 'competencesmapping'); ?>"><?php _e('Networking', 'competencesmapping'); ?></option>
																	<option value="<?php _e('Other projects', 'competencesmapping'); ?>"><?php _e('Other projects', 'competencesmapping'); ?></option>
																	<option value="<?php _e('Teaching as a SKF trainer', 'competencesmapping'); ?>"><?php _e('Teaching as a SKF trainer', 'competencesmapping'); ?></option>
																	<option value="<?php _e('Training (internal/external classroom)', 'competencesmapping'); ?>"><?php _e('Training (internal/external classroom)', 'competencesmapping'); ?></option>
																</select>
															</div>
														</div>
														<div class="col-sm-2">
															<div class="form-group">
																<select class="form-control" ng-model="filter.seniority_level">
																	<option value=""><?php _e('Seniority Level', 'competencesmapping'); ?></option>
																	<option value="<?php _e('Has to be driven', 'competencesmapping'); ?>"><?php _e('Has to be driven', 'competencesmapping'); ?> </option>
																	<option value="<?php _e('Follower', 'competencesmapping'); ?>"><?php _e('Follower', 'competencesmapping'); ?> </option>
																	<option value="<?php _e('Self Management', 'competencesmapping'); ?>"><?php _e('Self Management', 'competencesmapping'); ?> </option>
																	<option value="<?php _e('Reference', 'competencesmapping'); ?>"><?php _e('Reference', 'competencesmapping'); ?></option>
																</select>
															</div>
														</div>


														<div class="col-sm-2">
															<div class="form-group">
																<select class="form-control years_available" ng-model="filter.year" ng-options="model.year as model.year for model in years_available" ng-value="current_year">
																	<option value=""><?php _e('Year', 'competencesmapping'); ?></option>
																</select>
															</div>
														</div>


														<div class="col-sm-12 text-right">
															<hr style="border-color: #33333345;">
															<a class="btn" ng-click="reset_filter()"> <?php _e('reset', 'competencesmapping'); ?></a>
															<button class="btn btn-primary" type="submit" ng-click="search_filter()"><?php _e('Search', 'competencesmapping'); ?> <i class="fa fa-search"></i></button>
														</div>

													</div>
												</form>
											</div>



											<div class="results" ng-show="reports || reports.length">
												<b> <?php _e('Results', 'competencesmapping'); ?></b>
												<hr>

												<div class="alert alert-warning text-center" ng-show="reports && reports.length == 0">
													<strong><?php _e('Warning', 'competencesmapping'); ?>!</strong> <?php _e('No results', 'competencesmapping'); ?>
												</div>
												<div class="acoes" ng-show="reports.length > 0">
													<button class="btn btn-xs btn-info pull-right" ng-click="export(reports)"><i class="fa fa-file-excel-o"></i> <?php _e('Export to Excel', 'competencesmapping'); ?></button>
													<br><br>
													<div class="clearfix"></div>
												</div>

												<div class="table-responsive table-estilo-1" ng-show="reports.length > 0">

													<table id="table" class="table table-condensed table-bordered">


														<thead>
															<tr >
																<th style="min-width: 200px;" ng-repeat="c in reports_header track by $index">{{ c }}</th>

															</tr>
														</thead>
														<tbody>

															<tr ng-repeat="(key, report) in reports track by $index">
																<td ng-repeat="c in reports_header track by $index">{{ report[c] }}</td>

															</tr>

														</tbody>

													</table>
												</div>
												<div class="acoes" ng-show="reports.length > 0">
													<hr style="border-color: #33333345;">
													<button class="btn btn-xs btn-info pull-right" ng-click="export(reports)"><i class="fa fa-file-excel-o"></i> <?php _e('Export to Excel', 'competencesmapping'); ?></button>
												</div>
											</div>
										</div>
										<div id="box2-filter" class="tab-pane fade">
											<div class=" filter" >
												<form novalidate>
													<b> <?php _e('Filter', 'competencesmapping'); ?></b>
													<br><br>

													<div class="row">
														<div class="col-sm-4">
															<div class="form-group">
																<input type="text" class="form-control" ng-model="filter_180.name" placeholder="<?php _e('Name', 'competencesmapping'); ?>" title="<?php _e('Name', 'competencesmapping'); ?>">
															</div>
														</div>	

														<div class="col-sm-4">
															<div class="form-group">
																<input type="text" class="form-control" ng-model="filter_180.manage_name" placeholder="<?php _e('Manager Name', 'competencesmapping'); ?>" title="<?php _e('Manager Name', 'competencesmapping'); ?>">
															</div>
														</div>	

														<div class="col-sm-4">
															<div class="form-group">
																<input type="text" class="form-control" ng-model="filter_180.main_global_customer_contact" placeholder="<?php _e('Main Global Customer contact', 'competencesmapping'); ?>" title="<?php _e('Main Global Customer contact', 'competencesmapping'); ?>">
															</div>
														</div>	
														<div class="col-sm-12 text-right">
															<hr style="border-color: #33333345;">
															<a class="btn" ng-click="reset_filter_180()"> <?php _e('reset', 'competencesmapping'); ?></a>
															<button class="btn btn-primary" type="submit" ng-click="search_filter_180()"><?php _e('Search', 'competencesmapping'); ?> <i class="fa fa-search"></i></button>
														</div>

													</div>
												</form>
											</div>

											<div class="results" ng-show="reports_180 || reports_180.length">
												<b> <?php _e('Results', 'competencesmapping'); ?></b>
												<hr>

												<div class="alert alert-warning text-center" ng-show="reports_180 && reports_180.length == 0">
													<strong><?php _e('Warning', 'competencesmapping'); ?>!</strong> <?php _e('No results', 'competencesmapping'); ?>
												</div>
												<div class="acoes" ng-show="reports_180.length > 0">
													<button class="btn btn-xs btn-info pull-right" ng-click="export(reports_180)"><i class="fa fa-file-excel-o"></i> <?php _e('Export to Excel', 'competencesmapping'); ?></button>
													<br><br>
													<div class="clearfix"></div>
												</div>

												<div class="table-responsive table-estilo-1" ng-show="reports_180.length > 0">

													<table id="table2" class="table table-condensed table-bordered">
														<thead>
															<tr >
																<th style="min-width: 200px;" ng-repeat="c in reports_180_header track by $index">{{ c }}</th>

															</tr>
														</thead>
														<tbody>

															<tr ng-repeat="(key, report) in reports_180 track by $index">
																<td ng-repeat="c in reports_180_header track by $index">{{ report[c] }}</td>

															</tr>

														</tbody>
													</table>
												</div>
												<div class="acoes" ng-show="reports_180.length > 0">
													<hr style="border-color: #33333345;">
													<button class="btn btn-xs btn-info pull-right" ng-click="export(reports_180)"><i class="fa fa-file-excel-o"></i> <?php _e('Export to Excel', 'competencesmapping'); ?></button>
												</div>
											</div>
										</div>

									</div>

								</div>
								<div class="col-sm-3">
								</div>
							</div>
							<br>
							<div class="clearfix"></div>




						</div>

						<div id="menu-1" class="tab-pane fade " ng-show="perfilEnable('administrator')">

							<div class="row">
								<div class="col-sm-4">
									<br>
									<ul class="list-group" style="margin-bottom: 0px;">
										<li class="list-group-item" > 
											<span > <b>1° <?php _e('Competences', 'competencesmapping'); ?></b></span>

										</li>
									</ul>

									<div class="list-group scroll" style="height: 400px; max-height: 400px; border: none;">
										<a class="list-group-item" ng-show="competences && !competences.length"><?php _e('No values...', 'competencesmapping'); ?></a>
										<a class="list-group-item pointer" ng-click="select_competence(competence)" ng-class="{'active' : competence_selected.nameForm == competence.nameForm}" ng-repeat="competence in competences track by $index">
											{{ competence.nameForm  }} <i class="fa fa-angle-double-right pull-right"></i> 
										</a>

									</div>
									<hr>
									<div  class="bts">
										<button class="btn btn-primary pull-right" ng-click="add('add_competence')" data-toggle="modal" data-target="#myModal">+ <?php _e('add', 'competencesmapping'); ?></button> 
										<fieldset ng-disabled="!competence_selected.nameForm">
											<button class="btn btn-warning pull-right" ng-click="modify('modify_competence')" data-toggle="modal" data-target="#myModal">* <?php _e('modify', 'competencesmapping'); ?></button> 
											<button class="btn btn-danger pull-right" ng-click="delete_competence(competence_selected)">- <?php _e('delete', 'competencesmapping'); ?></button> 
										</fieldset>
									</div>
								</div>
								<div class="col-sm-4" ng-show="competence_selected.nameForm">
									<br>
									<ul class="list-group" style="margin-bottom: 0px;">
										<li class="list-group-item" > 
											<span > <b>2° {{ competence_selected.nameForm }} <?php _e('values', 'competencesmapping'); ?></b></span>

										</li>
									</ul>
									<div class="list-group scroll" style="height: 400px; max-height: 400px; border: none;">
										<a class="list-group-item" ng-show="competence_selected && !competence_selected.fields.length"><?php _e('No values...', 'competencesmapping'); ?></a>
										<a class="list-group-item pointer" ng-click="select_competence_field(field)" ng-class="{'active' : competence_field_selected == field}" ng-repeat="field in competence_selected.fields track by $index">{{ field }}</a>
									</div>
									<hr>
									<div class="bts">
										<button class="btn btn-primary pull-right" ng-click="add('add_competence_field')"  data-toggle="modal" data-target="#myModal">+ <?php _e('add', 'competencesmapping'); ?></button> 
										<fieldset ng-disabled="!competence_field_selected" >
											<button class="btn btn-warning pull-right" ng-click="modify('modify_competence_field')"  data-toggle="modal" data-target="#myModal">* <?php _e('modify', 'competencesmapping'); ?></button> 
											<button class="btn btn-danger pull-right" ng-click="delete_competence_field(competence_selected, competence_field_selected)">- <?php _e('delete', 'competencesmapping'); ?></button> 
										</fieldset>
									</div>

								</div>
								<div class="col-sm-4">
									<br>
									<ul class="list-group" style="margin-bottom: 0px;">
										<li class="list-group-item" style="padding-right: 33px;" ng-click="call_checkManagers(managers)"> 
											<span style="max-width: 83%;display: inline-block;"> <b>3° {{ competence_selected.nameForm }} <br> <?php _e('Managers authorized', 'competencesmapping'); ?></b></span>
											<div class="badge pointer" style="background: #0000; color: #333;  font-size: 19px; margin-top: 8px;">
												<i class="glyphicon glyphicon-unchecked" ng-class="{'glyphicon-unchecked' : !checkAllManagers, 'glyphicon-check' : checkAllManagers == true }" ></i>
											</div>
										</li>
									</ul>
									<div class="scroll" style="height: 400px; max-height: 380px; width: 100%; border: none;">
										<ul class="list-group">
											<li class="list-group-item" ng-show="managers && managers.length==0"> <?php _e('No managers here...', 'competencesmapping'); ?></li>
											<li class="list-group-item pointer flex flex-item" ng-repeat="manage1 in managers | orderBy:'display_name' track by $index" ng-click="call_checkManagers([manage1])" ng-class="{'active' : manage1.competence_autorized}">
												<span>{{ manage1.display_name }}</span>
												<div class="badge pointer" style="background: #0000; color: #333;  font-size: 19px;">
													<i class="glyphicon glyphicon-unchecked" ng-class="{'glyphicon-unchecked' : !manage1.competence_autorized, 'glyphicon-check' : manage1.competence_autorized  }" ></i>
												</div>
											</li>
										</ul>
									</div>
									<hr>
									<button class="btn btn-success pull-right" ng-click="saveManagersAuthorizeds()"><i class="fa fa-save"></i> <?php _e('save managers authorized', 'competencesmapping'); ?></button> 
								</div>
							</div>

							<!-- Modal -->
							<div class="modal fade" id="myModal" role="dialog">
								<div class="modal-dialog modal-sm">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h4 class="modal-title">Insert / Update</h4>
										</div>
										<div class="modal-body">
											<input type="text" class="form-control" ng-model="modal" placeholder="Insert here">
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-success" ng-click="save()" data-dismiss="modal">Save</button>
										</div>
									</div>
								</div>
							</div>

						</div>

						<div id="menu-1-1" class="tab-pane fade" ng-show="perfilEnable('administrator')">

							<b> <?php _e('Managers', 'competencesmapping'); ?></b>
							<hr>
							<div class="input-group">
								<input type="text" class="form-control" ng-model="filterField" placeholder="<?php _e('Search', 'competencesmapping'); ?>">
								<div class="input-group-addon">
									<i ng-hide="loadName" class="glyphicon glyphicon-search"></i>

								</div>
								
							</div>
							<hr>
							<div class="scroll">
								<ul class="list-group">
									<li class="list-group-item" ng-show="managers && managers.length==0"> <?php _e('No managers here...', 'competencesmapping'); ?></li>
									<li class="list-group-item" ng-repeat="manage in managers  | filter:{display_name:filterField}  | orderBy:'display_name' track by $index">
										<span>{{ manage.display_name }}</span>
										<button class="badge" title="Delete" ng-click="deleteManager(manage.ID)"><i class="fa fa-remove"></i></button>
									</li>
								</ul>
							</div>
							<hr>
							<form name="formAddManager" autocomplete="off" >
								<b> <?php _e('Add manage', 'competencesmapping'); ?></b>
								<hr>
								<div class="input-group">

									<input type="text" required class="form-control" required ng-model="newManager.login" placeholder="<?php _e('Login', 'competencesmapping'); ?>:" style="width: 33.3%;">
									<input type="text" required class="form-control" required ng-model="newManager.name" placeholder="<?php _e('Manager Name', 'competencesmapping'); ?>:" style="width: 33.3%;">
									<input type="password" required class="form-control" required ng-model="newManager.password" placeholder="<?php _e('Password', 'competencesmapping'); ?>:" style="width: 33.3%;">
									<div class="input-group-btn">
										<button class="pull-right btn btn-primary" ng-click="addManager(newManager, formAddManager.$valid)">+ <?php _e('Add Manage', 'competencesmapping'); ?></button>
									</div>
									
								</div>

							</form>

						</div>
						<div id="menu-1-2" class="tab-pane fade" ng-show="perfilEnable('administrator')">

							<b> <?php _e('Administrators', 'competencesmapping'); ?></b>
							<hr>
							<div class="input-group">
								<input type="text" class="form-control" ng-model="filterField1" placeholder="<?php _e('Search', 'competencesmapping'); ?>">
								<div class="input-group-addon">
									<i ng-hide="loadName" class="glyphicon glyphicon-search"></i>

								</div>
								
							</div>
							<hr>
							<div class="scroll">
								<ul class="list-group">
									<li class="list-group-item" ng-show="administrators && administrators.length==0"> <?php _e('No administrators here...', 'competencesmapping'); ?></li>
									<li class="list-group-item" ng-repeat="manage in administrators | filter:{display_name:filterField1} track by $index">
										<span>{{ manage.display_name }}</span>
										<button class="badge" title="Delete" ng-click="deleteAdministrator(manage.ID)"><i class="fa fa-remove"></i></button>
									</li>
								</ul>
							</div>
							<hr>
							<form name="formAddManager" autocomplete="off" action>
								<b> <?php _e('Add administrator', 'competencesmapping'); ?></b>
								<hr>
								<div class="input-group">									
									<input type="text" required class="form-control" required ng-model="newManager.login" placeholder="<?php _e('Login', 'competencesmapping'); ?>:" style="width: 33.3%;">
									<input type="text" required class="form-control" required ng-model="newManager.name" placeholder="<?php _e('Administrator Name', 'competencesmapping'); ?>:" style="width: 33.3%;">
									<input type="password" required class="form-control" required ng-model="newManager.password" placeholder="<?php _e('Password', 'competencesmapping'); ?>:" style="width: 33.3%;">
									<div class="input-group-btn">
										<button class="pull-right btn btn-primary" ng-click="addAdministrator(newManager, formAddManager.$valid)">+ <?php _e('Add Administrator', 'competencesmapping'); ?></button>
									</div>

								</div>

							</form>

						</div>


					</div>
				</div>


			</div>
		</div>







	</div>

	<div class="row">
		<div class="col-sm-12"  ng-if="isDev()">
	<pre>
		
{{ reports_180_header | json}}


	</pre>
		</div>
	</div>


</main>



<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/includes/table/xlsx.full.min.js?v=<?php echo get_rand(); ?>"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/controllers/App.js?v=<?php echo get_rand(); ?>"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/controllers/AdminController.js?v=<?php echo get_rand(); ?>"></script> 

<?php get_footer(); ?>