
<footer class="<?php echo is_front_page() ? 'absolute' : ''; ?>" >


	<div class="skf-footer">
		<div class="skf-logo">
			<span class="version">version: <?php echo wp_get_theme()->get( 'Version' ); ?></span>
		</div>
	</div>
</footer>

<div id="check-browser">
	<div class="browser-warning">
		<strong><?php _e('Navegador não suportado!', 'competencesmapping' ); ?>
		</strong><br>
		<?php _e('Para uma melhor experiência recomendamos o uso do Mozilla Firefox ou Google Chrome para aproveitar todos os recursos deste site.', 'competencesmapping' ); ?>
	</div>
</div>

<script type="text/javascript">

	function detect_browser() {
		var ua= navigator.userAgent, tem, M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
		if(/trident/i.test(M[1])){
			tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
			return 'IE '+(tem[1] || '');
		}
		if(M[1]=== 'Chrome'){
			tem= ua.match(/\b(OPR|Edge)\/(\d+)/);
			if(tem!= null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
		}
		M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
		if((tem= ua.match(/version\/(\d+)/i))!= null) M.splice(1, 1, tem[1]);
		return M.join(' ');
	};
	var current_browser = detect_browser();
			//console.log('running on '+current_browser);
			var browser_parts = current_browser.split(' ');
			var show_browser_warning = false;
			
			if ((browser_parts[0] == 'IE') || (browser_parts[0] == 'Opera') || (browser_parts[0] == 'Edge' && browser_parts[1] < 16) || (browser_parts[0] == 'Firefox' && browser_parts[1] < 52) || (browser_parts[0] == 'Chrome' && browser_parts[1] < 57) || (browser_parts[0] == 'Safari' && browser_parts[1] < 10) ) {
				show_browser_warning = true;
				$('.browser-warning').css( "display", "block" );
			}


		</script>

		<style type="text/css">

			:root {
				--warn-color-10: rgba(255,30,30,1);
				--warn-color-09: rgba(255,30,30,0.9);
				--warn-color-08: rgba(255,30,30,0.8);
				--warn-color-06: rgba(255,30,30,0.6);
				--warn-color-04: rgba(255,30,30,0.4);
				--warn-color-02: rgba(255,30,30,0.2);
				--warn-color-01: rgba(255,30,30,0.1);
				--warn-color-00: rgba(255,30,30,0);
			}

			@keyframes blink-warning {
				from {
					background-color: rgba(255,30,30,0.2);
					background-color: var(--warn-color-02);
				}
				to {
					background-color: rgba(255,30,30,0.6);
					background-color: var(--warn-color-06);
				}
			}
			
			.browser-warning {
				display: none; 
				text-align: center;
				background-color: #ff1e1e66;
				background-color: var(--warn-color-04);
				color: #fffffc;
				min-height: 90px;
				font-size: 16px;
				box-sizing: border-box;
				cursor: pointer;
				left: 0px;
				padding: 10px;
				z-index: 10000;
				bottom: 0px;
				margin-top: -85px;
				width: 100%;
				position: fixed;
				box-shadow: 0px 0px 10px 0px rgba(0,0,0,0);
				border: 1px solid #ff1e1ecc;
				border: 1px solid var(--warn-color-08);
				animation-name: blink-warning;
				animation-duration: 0.6s;
				animation-direction: alternate;
				animation-iteration-count: infinite;
			}

			.browser-warning strong {
				text-transform: uppercase;
			}
			@supports (not (display: grid)) or (not (object-fit: cover)) {
				.browser-warning {
					display: block;
				}
			}



		</style>

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/controllers/HeaderController.js?v=<?php echo get_rand(); ?>"></script>

</body>
</html>