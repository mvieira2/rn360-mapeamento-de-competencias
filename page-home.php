<?php get_header();  ?>
<h1 class="text-center" style="margin-bottom: 30px;">Competence mapping </h1>

<main ng-controller="Home" class="ng-cloak">
	<div class="container">
		<form novalidate name="formulario">
			<div class="row">
				<div class="col-sm-12">
					<div class="center">
						<ul class="nav nav-tabs" >
							<li ng-repeat="y in ::years_available track by $index" ng-class="{'active' : y.isCurrent == true,'disabled disabledTab' : y.year > current_year}">
								<a data-toggle="tab" href="#menu-{{y.year}}" ng-click="set_year_selected(y.year)">{{ y.year }} </a>
							</li>
						</ul>
					</div>
					<br>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="form-init-list center">
						<div class="panel-group" id="accordion" ng-show="menuId < formItens.length+2">
							<br>
							<div class="panel panel-default" ng-class="{'isOk' : filtered2.length}">
								<div class="panel-heading" ng-click="setFormItem(0)">
									<a data-toggle="collapse" data-parent="#accordion" href="#menu-0" >
										<h4 class="panel-title">
											<b>1 - <?php _e('Profile', 'competencesmapping'); ?></b>
											<i class="icon-header fa fa-thumbs-up pull-right"></i>
										</h4>
									</a>
								</div>
								<div id="menu-0" class="panel-collapse collapse in" disabled-ng-class="{'in height-auto' : menuId == 0}">
									<div class="panel-body">

										<div  class="form-0">
											<div class="col-sm-12 text-right">
												<small style="color: #bbb4b4;" ng-show="last_update"> <?php _e('Last update', 'competencesmapping'); ?>: {{ last_update | date : "dd/MM/yy hh:mm:ss"}}</small>
											</div>
											<div class="col-sm-6">
												<div class="form-group">
													<label> <?php _e('Appraiser', 'competencesmapping'); ?></label>
													<div class="input-group">
														<div class="input-group-addon"> <i class="fa fa-user"></i> </div>
														<input ng-disabled="year_selected != current_year || perfilEnable('subscriber')" type="text" ng-model="answers.profile.appraiser_name" class="form-control" ng-model-options="{updateOn: 'default blur',debounce: { 'default': 500, 'blur': 0 }}" title="<?php _e('Appraiser Name', 'competencesmapping'); ?>" placeholder="<?php _e('Appraiser Name', 'competencesmapping'); ?>:">

													</div>
												</div>
											</div>
											<div class="col-sm-12">
												<hr>
											</div>
											<div class="col-sm-6">
												<div class="input-group">
													<input type="text" autocomplete="off" ng-model="answers.profile.name" ng-disabled="perfilEnable('subscriber')" class="form-control" ng-change="applyChange()" id="project" placeholder="<?php _e('Name', 'competencesmapping'); ?>:" title="<?php _e('Name', 'competencesmapping'); ?>:">
													<div class="input-group-addon">
														<i ng-show="loadName" class="fa fa-circle-o-notch fa-fw fa-spin"></i>
														<i ng-hide="loadName" class="glyphicon glyphicon-search"></i>

													</div>
												</div>
											</div>
											<div class="col-sm-6" ng-show="year_selected == current_year && existImport">

												<a class="pointer" ng-click="importFromLastYear( answers.profile.email )"><?php _e('Import from last year', 'competencesmapping'); ?><i class="fa fa-level-down"></i></a>
											</div>
											<fieldset ng-disabled="year_selected != current_year || perfilEnable('subscriber')" style="width: 100%;">	
												<div class="clearfix"> </div> <br>

												<div class="col-sm-6" ng-class="{'has-error' : answers.profile.ID && formulario.email1.$invalid }">
													<div class="form-group">
														<div class="input-group">
															<div class="input-group-addon"> <i class="fa fa-envelope"></i> </div>
															<input type="email" required ng-model="answers.profile.email" name="email1" class="form-control" placeholder="<?php _e('E-mail', 'competencesmapping'); ?>:" title="<?php _e('E-mail', 'competencesmapping'); ?>:">
														</div>
													</div>
												</div>
												<div class="col-sm-6">
													<div class="form-group">
														<div class="input-group">
															<div class="input-group-addon"> <i class="fa fa-calendar"></i> </div>
															<input type="text" ng-model="answers.profile.screening_date" class="form-control date" ng-change="applyChange()" id="date"  placeholder="<?php _e('Screening Date', 'competencesmapping'); ?>:" title="<?php _e('Screening Date', 'competencesmapping'); ?>:">
														</div>
													</div>
												</div>

												<div class="col-sm-6">
													<div class="form-group">
														<div class="input-group">
															<div class="input-group-addon"> <i class="	fa fa-user"></i> </div>
															<select class="form-control" title="<?php _e('Manager Name', 'competencesmapping'); ?>" ng-model="answers.profile.manage_name" ng-options="m.user_nicename as m.display_name for m in managers  | orderBy:'display_name'" ng-change="get_competences_itens(answers.profile.manage_name)">
																<option value="" ng-selected="answers.profile.user_nicename==''"> <?php _e('Manager Name', 'competencesmapping'); ?>: </option>
															</select>
														</div>
													</div>

												</div>
												<div class="col-sm-3">
													<div class="form-group">
														<div class="input-group">
															<div class="input-group-addon"> <i class="	fa fa-briefcase"></i> </div>
															<input type="text" ng-model="answers.profile.current_position" class="form-control" id="position" placeholder="<?php _e('Current Position', 'competencesmapping'); ?>:" title="<?php _e('Current Position', 'competencesmapping'); ?>:">
														</div>
													</div>
												</div>
												<div class="col-sm-3">
													<div class="form-group">
														<div class="input-group">
															<div class="input-group-addon"> <i class="fa fa-address-card"></i> </div>
															<input type="text" ng-model="answers.profile.document_number" class="form-control" id="position" placeholder="<?php _e('CPF, ID, Foreign Card', 'competencesmapping'); ?>:" title="<?php _e('CPF, ID, Foreign Card', 'competencesmapping'); ?>:">
														</div>
													</div>
												</div>

												<div class="col-sm-6">
													<div class="form-group">
														<div class="input-group">
															<div class="input-group-addon"> <i class="fa fa-globe"></i> </div>
															<input type="text" ng-model="answers.profile.main_global_customer_contact" class="form-control" placeholder="<?php _e('Main Global Customer contact', 'competencesmapping'); ?>:" title="<?php _e('Main Global Customer contact', 'competencesmapping'); ?>:">
														</div>
													</div>
												</div>

												<div class="col-sm-6">
													<div class="form-group">
														<div class="input-group">
															<div class="input-group-addon"> <i class="fa fa-hourglass-2"></i> </div>
															<select ng-model="answers.profile.time_in_current_job_role" class="form-control" title="<?php _e('Time in current job role', 'competencesmapping'); ?>">
																<option value="" ><?php _e('Time in current job role', 'competencesmapping'); ?>:</option>
																<option value="<?php _e('Junior (1 to 3 years)', 'competencesmapping'); ?>"><?php _e('Junior (1 to 3 years)', 'competencesmapping'); ?></option>
																<option value="<?php _e('Full (4 to 6 years)', 'competencesmapping'); ?>"><?php _e('Full (4 to 6 years)', 'competencesmapping'); ?></option>
																<option value="<?php _e('Senior (6 to 10 years)', 'competencesmapping'); ?>"><?php _e('Senior (6 to 10 years)', 'competencesmapping'); ?></option>
																<option value="<?php _e('Master  (more 10 years)', 'competencesmapping'); ?>"><?php _e('Master  (more 10 years)', 'competencesmapping'); ?></option>
															</select>
														</div>
													</div>
												</div>
												<div class="col-sm-6">
													<div class="form-group">
														<div class="input-group">
															<div class="input-group-addon"> <i class="fa fa-language"></i> </div>
															<select class="form-control" ng-model="answers.profile.mother_tongue" title="<?php _e('Mother Language', 'competencesmapping'); ?>">
																<option  value="" selected="selected"><?php _e('Mother Language', 'competencesmapping'); ?>:</option>
																<option value="<?php _e('English', 'competencesmapping'); ?>"><?php _e('English', 'competencesmapping'); ?></option>
																<option value="<?php _e('Spanish', 'competencesmapping'); ?>"><?php _e('Spanish', 'competencesmapping'); ?></option>
																<option value="<?php _e('Portuguese', 'competencesmapping'); ?>"><?php _e('Portuguese', 'competencesmapping'); ?></option>
															</select>

														</div>
													</div>
												</div>

												<div class="col-sm-6">
													<div class="form-group" style="
													display: flex;
													min-height: 36px;
													">
													<label style="margin: 8px;"  title="<?php _e('SKF employee?', 'competencesmapping'); ?>"><?php _e('SKF employee?', 'competencesmapping'); ?></label> 
													<div class="botao" style="margin: 4px;">
														<input type="checkbox" id="toggleStatus" class="toggle" ng-model="answers.profile.skf_employee" ng-checked="answers.profile.skf_employee=='Yes'" ng-true-value="'Yes'" ng-false-value="'No'">
														<label for="toggleStatus" title="{{answers.profile.skf_employee}}"></label>
													</div>

												</div>
											</div>
											<div class="col-sm-6">
												<div class="form-group">
													<div class="input-group">
														<div class="input-group-addon"> <i class="fa fa-phone"></i> </div>
														<input type="tex" ng-model="answers.profile.phone" class="form-control" placeholder="<?php _e('Phone', 'competencesmapping'); ?>:" title="<?php _e('Phone', 'competencesmapping'); ?>:">
													</div>
												</div>
											</div>
											<div class="col-sm-2">
												<div class="form-group">
													<div class="input-group">
														<div class="input-group-addon"> <i class="fa fa-map-o"></i> </div>
														<input type="text" ng-model="answers.profile.country" class="form-control" placeholder="<?php _e('Country', 'competencesmapping'); ?>:" title="<?php _e('Country', 'competencesmapping'); ?>:">
													</div>
												</div>
											</div>
											<div class="col-sm-2">
												<div class="form-group">
													<div class="input-group">
														<div class="input-group-addon"> <i class="fa fa-map"></i> </div>
														<input type="text" ng-model="answers.profile.city" class="form-control" placeholder="<?php _e('City', 'competencesmapping'); ?>:" title="<?php _e('City', 'competencesmapping'); ?>:">
													</div>
												</div>
											</div>
											<div class="col-sm-2">
												<div class="form-group">
													<div class="input-group">
														<div class="input-group-addon"> <i class="fa fa-map-o"></i> </div>
														<input type="text" ng-model="answers.profile.state_or_province" class="form-control" placeholder="<?php _e('State or Province', 'competencesmapping'); ?>:" title="<?php _e('State or Province', 'competencesmapping'); ?>:">
													</div>
												</div>
											</div>
											<div class="col-sm-12">
												<h3> <?php _e('Experience', 'competencesmapping'); ?></h3>
											</div>
											<div class="col-sm-6">
												<div class="form-group">
													<div class="input-group">
														<div class="input-group-addon"> <i class="fa fa-black-tie"></i> </div>
														<select ng-model="answers_edit.profile.experience.segment_experience" class="form-control" title="<?php _e('Segment experience', 'competencesmapping'); ?>">
															<option value=""><?php _e('Segment experience', 'competencesmapping'); ?>:</option>
															<option value="<?php _e('Oil & Gas', 'competencesmapping'); ?>"><?php _e('Oil & Gas', 'competencesmapping'); ?></option>
															<option value="<?php _e('Pulp & Paper', 'competencesmapping'); ?>"><?php _e('Pulp & Paper', 'competencesmapping'); ?></option>
															<option value="<?php _e('Metal Work', 'competencesmapping'); ?>"><?php _e('Metal Work', 'competencesmapping'); ?></option>
															<option value="<?php _e('Food & Beverage', 'competencesmapping'); ?>"><?php _e('Food & Beverage', 'competencesmapping'); ?></option>
															<option value="<?php _e('Mining', 'competencesmapping'); ?>"><?php _e('Mining', 'competencesmapping'); ?></option>
															<option value="<?php _e('Marine', 'competencesmapping'); ?>"><?php _e('Marine', 'competencesmapping'); ?></option>
															<option value="<?php _e('Cement', 'competencesmapping'); ?>"><?php _e('Cement', 'competencesmapping'); ?></option>
															<option value="<?php _e('Chemical', 'competencesmapping'); ?>"><?php _e('Chemical', 'competencesmapping'); ?></option>
															<option value="<?php _e('Wind power', 'competencesmapping'); ?>"><?php _e('Wind power', 'competencesmapping'); ?></option>
															<option value="<?php _e('Agricultural', 'competencesmapping'); ?>"><?php _e('Agricultural', 'competencesmapping'); ?></option>
															<option value="<?php _e('Sugar & Ethanol', 'competencesmapping'); ?>"><?php _e('Sugar & Ethanol', 'competencesmapping'); ?></option>

														</select>
													</div>
												</div>
											</div>
											<div class="col-sm-6">
												<div class="form-group">
													<div class="input-group">
														<div class="input-group-addon"> <i class="fa fa-hourglass"></i> </div>
														<select ng-model="answers_edit.profile.experience.experience" class="form-control" title="<?php _e('Experience', 'competencesmapping'); ?>">
															<option value=""><?php _e('Experience', 'competencesmapping'); ?></option>
															<option value="<?php _e('Junior', 'competencesmapping'); ?>"><?php _e('Junior (1 to 3 years)', 'competencesmapping'); ?></option>
															<option value="<?php _e('Full', 'competencesmapping'); ?>"><?php _e('Full (4 to 6 years)', 'competencesmapping'); ?></option>
															<option value="<?php _e('Senior', 'competencesmapping'); ?>"><?php _e('Senior (6 to 10 years)', 'competencesmapping'); ?></option>
															<option value="<?php _e('Master', 'competencesmapping'); ?>"><?php _e('Master (more 10 years)', 'competencesmapping'); ?></option>
														</select>
													</div>
												</div>
											</div>
											<div class="col-sm-12 text-right">

												<button class="btn btn-primary ng-binding animated" ng-disabled="!answers_edit.profile.experience.segment_experience || !answers_edit.profile.experience.experience " ng-class="{'heartBeat' : answers_edit.profile.experience.segment_experience &&  answers_edit.profile.experience.experience}" ng-click="addExperience(answers_edit, 'profile')"><?php _e('+ Experience', 'competencesmapping'); ?></button>
											</div>

											<div class="col-sm-12" ng-hide="!!!filtered2.length">
												<br>
												<div class="table-responsive ">

													<table class="table table-striped border1">
														<thead>
															<tr>
																<th class="text-center"><?php _e('Segment experience', 'competencesmapping'); ?></th>
																<th class="text-center"><?php _e('Experience', 'competencesmapping'); ?></th>
																<th class="text-center"></th>
															</tr>
														</thead>
														<tbody>
															<tr ng-show="!!!filtered2.length">
																<td colspan="7" class="text-center" >
																	<b class="text-danger"><?php _e('No added', 'competencesmapping'); ?></b>
																</td>
															</tr>
															<tr ng-repeat="(key3, experience) in answers.profile.experience as filtered2 track by $index " class="text-center" ng-class="{'warning' : false}">
																<td>{{ experience.segment_experience || '-' }}</td>
																<td>{{ experience.experience || '-' }}</td>
																<td class="actions">
																	<div ng-hide="year_selected != current_year">
																		<button class="badge" title="Delete" ng-click="deleteExperience(experience, 'profile')">
																			<i class="fa fa-remove"></i>
																		</button>
																	</div>
																</td>
															</tr>
														</tbody>
													</table>
												</div>
											</div>


											<div class="col-sm-12">
												<hr>
												<button class="btn pull-left " ng-hide="menuId==0" ng-click="setFormItem()"> <i class="fa fa-angle-double-left"></i> <?php _e('Back', 'competencesmapping'); ?>  </button>

												<button class="btn btn-success pull-right " ng-click="setFormItem(1)"><?php _e('Next', 'competencesmapping'); ?> <i class="fa fa-angle-double-right"></i> </button>
											</div>

										</fieldset>
									</div>
								</div>
							</div>
						</div>

						<div class="panel panel-default form-{{ formItem.ID }} {{ formItem.slug }}" ng-repeat="(key, formItem) in formItens track by formItem.ID " ng-class="{'isOk' : filtered.length, 'locked' : !answers.profile.name}">
							<fieldset ng-disabled="year_selected != current_year || perfilEnable('subscriber')">	
								<div class="panel-heading" ng-click="setFormItem(key+1)">
									<a data-toggle="collapse" data-parent="#accordion" href="#menu-{{ key + 1 }}">
										<h4 class="panel-title">
											<b >{{ key + 2}} - {{ formItem.nameForm }}</b>
											<i class="icon-header fa fa-thumbs-up pull-right"></i>
										</h4>
									</a>
								</div>
								<div id="menu-{{ key + 1 }}" class="menuList panel-collapse  collapse {{ key +''+ menuId }}" disabled-ng-class="{'in height-auto' : key + 1 == menuId}" >
									<div class="panel-body">
										<div ng-show="perfilEnable('subscriber') && filtered && filtered.length == 0">
											<?php _e('No results', 'competencesmapping'); ?>
										</div>

										<div ng-show="!perfilEnable('subscriber')">
											<div class="col-sm-12">
												<hr>
											</div>
											<div class="col-sm-6">
												<div class="form-group" title="{{ formItem.nameForm }}">
													<label> {{ formItem.nameForm }}</label>

													<select class="form-control" ng-model="answers_edit.competences[ formItem.slug ][ formItem.slug ]"  ng-options="model as model for model in formItem.fields" >
														<option value="">{{ formItem.nameForm }}:</option>
													</select>

												</div>
											</div>
											<div class="col-sm-12">
												<div class="row">
													<div class="col-sm-12">
														<hr>
														<label>Knowledge Level <a href="#" data-toggle="popover" data-trigger="focus" data-content="<b>0</b> = any Knowledge; <br> <b>1</b> = basic knowledge;  <br> <b>2</b> = intermediate knowledge; <br> <b>3</b> = advanced knowledge;"><i class="fa fa-question-circle"></i></a></label>
													</div>
													<div class="col-sm-4">
														<div class="form-group" title="<?php _e('Expected', 'competencesmapping'); ?>">
															<label ><?php _e('Expected', 'competencesmapping'); ?>: </label>
															{{ answers_edit.competences[ formItem.slug ].expected }}
															<input type="range" value="0" class="form-control" ng-model="answers_edit.competences[ formItem.slug ].expected" min="0" max="5">
														</div>
													</div>
													<div class="col-sm-4">

														<div class="form-group" title="<?php _e('Reached', 'competencesmapping'); ?>">
															<label><?php _e('Reached', 'competencesmapping'); ?>: </label>
															{{ answers_edit.competences[ formItem.slug ].reached }}
															<input type="range" value="0" class="form-control" ng-model="answers_edit.competences[ formItem.slug ].reached" min="0" max="5">
														</div>
													</div>

													<div class="col-sm-4">
														<div class="form-group" title="<?php _e('Priority', 'competencesmapping'); ?>">
															<label><?php _e('Priority', 'competencesmapping'); ?>: </label>
															{{ answers_edit.competences[ formItem.slug ].priority }}
															<input type="range" value="0" class="form-control" ng-model="answers_edit.competences[ formItem.slug ].priority" min="0" max="5">
														</div>
													</div>
													<div class="col-sm-12"><hr></div>
												</div>
											</div>

											<div class="col-sm-6">
												<div class="row">
													<div class="col-xs-6">
														<div class="form-group">
															<label> <?php _e('Development', 'competencesmapping'); ?> </label>
															<select class="form-control" ng-model="answers_edit.competences[ formItem.slug ].development" >
																<option value=""><?php _e('Suggestion of Action', 'competencesmapping'); ?>:</option>
																<option value="<?php _e('Books / Journal', 'competencesmapping'); ?>"><?php _e('Books / Journal', 'competencesmapping'); ?></option>
																<option value="<?php _e('Coaching', 'competencesmapping'); ?>"><?php _e('Coaching', 'competencesmapping'); ?></option>
																<option value="<?php _e('Conference', 'competencesmapping'); ?>"><?php _e('Conference', 'competencesmapping'); ?></option>
																<option value="<?php _e('Customer visit', 'competencesmapping'); ?>"><?php _e('Customer visit', 'competencesmapping'); ?></option>
																<option value="<?php _e('Exhibition', 'competencesmapping'); ?>"><?php _e('Exhibition', 'competencesmapping'); ?></option>
																<option value="<?php _e('Job rotation', 'competencesmapping'); ?>"><?php _e('Job rotation', 'competencesmapping'); ?></option>
																<option value="<?php _e('Learning on the job', 'competencesmapping'); ?>"><?php _e('Learning on the job', 'competencesmapping'); ?></option>
																<option value="<?php _e('Mentoring', 'competencesmapping'); ?>"><?php _e('Mentoring', 'competencesmapping'); ?></option>
																<option value="<?php _e('Networking', 'competencesmapping'); ?>"><?php _e('Networking', 'competencesmapping'); ?></option>
																<option value="<?php _e('Other projects', 'competencesmapping'); ?>"><?php _e('Other projects', 'competencesmapping'); ?></option>
																<option value="<?php _e('Teaching as a SKF trainer', 'competencesmapping'); ?>"><?php _e('Teaching as a SKF trainer', 'competencesmapping'); ?></option>
																<option value="<?php _e('Training (internal/external classroom)', 'competencesmapping'); ?>"><?php _e('Training (internal/external classroom)', 'competencesmapping'); ?></option>
															</select>

														</div>
													</div>
													<div class="col-xs-6">
														<div class="form-group">
															<label> <?php _e('Suggestion', 'competencesmapping'); ?> </label>
															<input type="text" autofocus ng-model="answers_edit.competences[ formItem.slug ].development_suggestion" class="form-control" placeholder="Enter here  {{ answers_edit.competences[ formItem.slug ].development}}">
														</div>
													</div>
												</div>

											</div>
											<div class="col-sm-6">
												<div class="form-group">
													<label> <?php _e('Seniority Level', 'competencesmapping'); ?>  <a href="#" data-toggle="popover" data-trigger="focus" data-content="<?php esc_attr_e('<b>Has to be driven</b> - requires specific and detailed instructions <br> <b>Follower</b> - requires general guidance <br> <b>Self Management</b> - no require supervision <br> <b>Reference</b> - Considered as Specialist in this Job Family', 'competencesmapping'); ?>"><i class="fa fa-question-circle"></i></a> </label>
													<select class="form-control" ng-model="answers_edit.competences[ formItem.slug ].seniority_level">
														<option value=""><?php _e('Seniority Level', 'competencesmapping'); ?>:</option>
														<option value="<?php _e('Has to be driven', 'competencesmapping'); ?>"><?php _e('Has to be driven', 'competencesmapping'); ?> </option>
														<option value="<?php _e('Follower', 'competencesmapping'); ?>"><?php _e('Follower', 'competencesmapping'); ?> </option>
														<option value="<?php _e('Self Management', 'competencesmapping'); ?>"><?php _e('Self Management', 'competencesmapping'); ?> </option>
														<option value="<?php _e('Reference', 'competencesmapping'); ?>"><?php _e('Reference', 'competencesmapping'); ?></option>
													</select>
												</div>
											</div>
											<div class="col-sm-12 text-right">

												<button class="btn btn-primary animated" ng-class="{'heartBeat' : answers_edit.competences[ formItem.slug ][ formItem.slug ] }"  ng-click="addAnswers(answers_edit, formItem.slug)">{{ answers_edit["competences"][formItem.slug]["edited"] ? 'Save' : '+ ' +formItem.nameForm }}</button>
											</div>
										</div>
										<div class="col-sm-12" ng-show="filtered.length">
											<hr>

											<div class="table-responsive ">

												<table class="table table-striped border1">
													<thead>
														<tr>
															<th class="text-center">{{ formItem.nameForm  }}</th>
															<th class="text-center"><?php _e('Expected', 'competencesmapping'); ?></th>
															<th class="text-center"><?php _e('Reached', 'competencesmapping'); ?></th>
															<th class="text-center"><?php _e('Priority', 'competencesmapping'); ?></th>
															<th class="text-center"><?php _e('Development', 'competencesmapping'); ?></th>
															<th class="text-center"><?php _e('Seniority Level', 'competencesmapping'); ?></th>
															<th class="text-center"></th>
														</tr>
													</thead>
													<tbody>
														<tr ng-show="!!!filtered.length">
															<td colspan="7" class="text-center" >
																<b class="text-danger">No {{ formItem.nameForm }} added</b>
															</td>
														</tr>
														<tr ng-repeat="(key3, question) in answers.competences | filterBy:formItem.slug as filtered track by $index " class="text-center" ng-class="{'warning' : question.edited}">
															<td>{{ question.competence_selected || '-' }} </td>
															<td>{{ question.expected || '-' }}</td>
															<td>{{ question.reached || '-' }}</td>
															<td>{{ question.priority || '-' }}</td>
															<td>{{ question.development   || '-' }} {{ question.development_suggestion ? "- "+ question.development_suggestion  : '' }}</td>
															<td>{{ question.seniority_level || '-' }}</td>
															<td class="actions">
																<div ng-hide="year_selected != current_year">
																	<button class="badge" title="Delete" ng-click="deleteAnswers(question, formItem.slug)">
																		<i class="fa fa-remove"></i>
																	</button>
																	<button class="badge" title="Edit" ng-click="editAnswers(question, answers_edit, formItem.slug, $index)">
																		<i class="fa fa-edit"></i>
																	</button>
																</div>


															</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>

										<div class="row" ng-show="!perfilEnable('subscriber')">
											<div class="col-sm-12">
												<hr>
												<button class="btn pull-left " ng-hide="menuId==0" ng-click="setFormItem(key)"> <i class="fa fa-angle-double-left"></i> Back  </button>

												<button class="btn btn-success pull-right " ng-disabled="!!!filtered.length"  ng-click="setFormItem(key+2)">Next <i class="fa fa-angle-double-right"></i> </button>
												<a class="btn  pull-right " ng-click="setFormItem(key+2)">Skip   </a>
											</div>
										</div>
									</div>
								</div>
							</fieldset>
						</div>

						<div class="panel panel-default" ng-class="{'locked' : !answers.profile.name}">
							<fieldset ng-disabled="year_selected != current_year || perfilEnable('subscriber')">	
								<div class="panel-heading" ng-click="setFormItem( formItens.length+1 )">
									<a data-toggle="collapse" data-parent="#accordion" href="#menu-{{ formItens.length+1 }}" >
										<h4 class="panel-title">
											<b>{{ formItens.length+2 }} - Individual Report - Confirm </b>
											<i class="icon-header fa fa-thumbs-up pull-right"></i>
										</h4>
									</a>
								</div>
								<div id="menu-{{ formItens.length+1 }}" class="menuList panel-collapse  collapse  ">
									<div class="panel-body">
										<div class="row graphics">
											<div class="col-sm-12">
												<h2 class="text-center">Individual Report - Confirm</h2>
												<hr>
											</div>
											<div class="col-sm-12">

												<div class="form-horizontal">
													<div class="center-1">

														<!-- loop dos campos profile ja preenchidos -->
														<div class="form-group" ng-repeat="field in resumeAllFieldsProfile track by $index" ng-hide="field.name=='ID'">
															<label class="control-label col-sm-4">{{ field.name }}:</label>
															<div class="col-sm-8">
																<p class="form-control-static">{{ field.val || '-' }}</p>
															</div>
														</div>

														<hr>
														<div class="table-responsive ">

															<table class="table table-striped border1">
																<thead>
																	<tr >
																		<th class="text-center"><?php _e('Competence', 'competencesmapping'); ?></th>
																		<th class="text-center"><?php _e('Selected', 'competencesmapping'); ?></th>
																		<th class="text-center"><?php _e('Expected', 'competencesmapping'); ?></th>
																		<th class="text-center"><?php _e('Reached', 'competencesmapping'); ?></th>
																		<th class="text-center"><?php _e('Priority', 'competencesmapping'); ?></th>
																		<th class="text-center"><?php _e('Development', 'competencesmapping'); ?></th>
																		<th class="text-center"><?php _e('Seniority Level', 'competencesmapping'); ?></th>
																	</tr>
																</thead>
																<tbody>
																	<tr ng-show="!!!answers.competences.length">
																		<td colspan="8" class="text-center" >
																			<b class="text-danger"><?php _e('No', 'competencesmapping'); ?> {{ formItem.nameForm }} <?php _e('added', 'competencesmapping'); ?></b>
																		</td>
																	</tr>
																	<tr ng-repeat="(key3, question) in answers.competences track by $index " class="text-center" ng-class="{'warning' : question.edited}">
																		<td>{{ question.competence_name || '-' }} </td>
																		<td>{{ question.competence_selected || '-' }} </td>
																		<td>{{ question.expected || '-' }}</td>
																		<td>{{ question.reached  || '-'}}</td>
																		<td>{{ question.priority || '-' }}</td>
																		<td>{{ question.development   || '-' }} {{ question.development_suggestion ? "- "+ question.development_suggestion  : '' }}</td>
																		<td>{{ question.seniority_level || '-' }}</td>

																	</tr>
																</tbody>
															</table>
														</div>
													</div>


												</div>
											</div>
											<div class="col-sm-12">
												<div class="row graphics">
													<div class="col-sm-12">
														<br>
														<hr>
														<div class="center">
															<label>PMP:</label>
															<select class="form-control" ng-model="answers.profile.pmp" --dng-disabled="answers.profile.pmp" >
																<option value="">---</option>
																<option value="<?php _e('Unsatisfactory', 'competencesmapping'); ?>"><?php _e('Unsatisfactory', 'competencesmapping'); ?></option>
																<option value="<?php _e('Partial Achievement', 'competencesmapping'); ?>"><?php _e('Partial Achievement', 'competencesmapping'); ?></option>
																<option value="<?php _e('Full Achievement', 'competencesmapping'); ?>"><?php _e('Full Achievement', 'competencesmapping'); ?></option>
																<option value="<?php _e('Higher Achievement', 'competencesmapping'); ?>"><?php _e('Higher Achievement', 'competencesmapping'); ?></option>
																<option value="<?php _e('Top Achievement', 'competencesmapping'); ?>"><?php _e('Top Achievement', 'competencesmapping'); ?></option>
															</select>
														</div>
													</div>
													<div class="col-sm-12">

														<hr>
														<h2 class="text-center">180º <?php _e('Assessment', 'competencesmapping'); ?></h2>
														<ul class="nav nav-tabs hide">
															<li class="active"><a data-toggle="tab" href="#menu0">2019</a></li>
															<li><a data-toggle="tab" href="#menu1">2020</a></li>
															<li><a data-toggle="tab" href="#menu2">2021</a></li>
														</ul>
														<br>
													</div>
													<div class="col-sm-12">

														<div class="tab-content">
															<div id="menu0" class="tab-pane fade in active">
																<br>
																<div class="row">
																	<div class="col-sm-4"></div>
																	<div class="col-sm-6" style="padding-bottom: 10px;"  ng-show="perfilEnable('subscriber') ">
																		<div class="row text-center">
																			<div class="col-xs-2 text-left">0</div>
																			<div class="col-xs-2">1</div>
																			<div class="col-xs-2">2</div>
																			<div class="col-xs-2">3</div>
																			<div class="col-xs-2">4</div>
																			<div class="col-xs-2">5</div>
																		</div>
																	</div>
																	<div class="clearfix"></div>
																	<div class="graphic-line"  ng-repeat="graphic in graphics track by $index" >
																		<div class="col-sm-4 text-right">
																			{{ graphic.name }}
																		</div>
																		<div class="col-sm-6">
																			<div class="progress" ng-show="perfilEnable('subscriber') ">
																				<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40"
																				aria-valuemin="0" aria-valuemax="100"  ng-style="{'width': ((graphic.val / 5 * 100) )+'%', 'color': getColor(graphic.val).color, 'background-color': getColor(graphic.val).backgroundColor }"

																				>
																				{{ graphic.val }}
																			</div>
																		</div>

																		<input type="number" min="0" max="5" class="form-control" ng-model="graphic.val" ng-show="!assessment.hasOwnProperty('ID') || perfilEnable('administrator') || perfilEnable('manager')" ng-hide="perfilEnable('subscriber')">

																	</div>
																	<div class="clearfix" style="margin-bottom: 10px;"></div>
																</div>
															</div>

														</div>
														<div id="menu1" class="tab-pane fade">
															<h4> <?php _e('No results', 'competencesmapping'); ?></h4>
														</div>
														<div id="menu2" class="tab-pane fade">
															<h4><?php _e('No results', 'competencesmapping'); ?></h4>
														</div>
													</div>


												</div>

											</div>
										</div>
										<div class="col-sm-12">
											<div ng-hide="year_selected != current_year">
												<hr>
												<button class="btn pull-left" ng-hide="menuId==0" ng-click="setFormItem( formItens.length )"> <i class="fa fa-angle-double-left"></i> <?php _e('Back', 'competencesmapping'); ?>  </button>
												<button class="btn btn-success pull-right " ng-click="save(answers)" ><?php _e('Confirm send', 'competencesmapping'); ?> <i class="fa fa-upload"></i> </button>

											</div>
										</div>

									</div>
								</div>
							</div>
						</fieldset>
					</div>
				</div> 
			</div>
		</div>
	</div>
</form>
</div>

<div class="row">
	<div class="col-sm-12"  ng-if="isDev()">

	</div>
</div>


</main>



<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/controllers/App.js?v=<?php echo get_rand(); ?>"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/controllers/HomeController.js?v=<?php echo get_rand(); ?>"></script>

<?php get_footer(); ?>