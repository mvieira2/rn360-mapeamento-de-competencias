app.controller('Home', function($scope, $rootScope, $location,$timeout, API) {

	$scope.menuId = 0;
	$scope.answers_edit = {};
	$scope.answers = { profile : { name: '', appraiser_name : '', experience : []}, competences : [] };
	$scope.resumeAllFieldsProfile = [];  // Lista os campos do profile no resumo
	$scope.formItens = [];
	$scope.years_available = [];
	$scope.existImport = false;
	$scope.year_selected = new Date().getFullYear();
	$scope.current_year = new Date().getFullYear();

	$scope.perfilName = [];

	$scope.init = async function () {

		$scope.resetForm();
		$scope.get_years_available();
		$scope.perfilName = await API.post("get_perfil");
		$scope.get_managers();

		if ($scope.perfilEnable('subscriber')) {

			$scope.loadName = true;
			let result = await API.get("users?search&isColaborador=1");
			$scope.loadName = false;

			$scope.setColaborador( result[0] );
			$scope.get_competences_itens( $scope.answers.profile.manage_name );
			$scope.$apply()
		}

	}
	
	$scope.resetForm = function (menuId) {

		$scope.answers = { profile : { name: '', experience : []}, competences : [] };
		$scope.existImport = false;
		$scope.last_update = null;
		$scope.assessment = {};

		$scope.formItens = [];

		$scope.graphics = [{
			name : 'Outside - in',
			val : 0
		},{
			name : 'Accountability',
			val : 0
		},{
			name : 'Empowerment',
			val : 0
		},{
			name : 'Teamwork',
			val : 0
		},{
			name : 'Openness',
			val : 0
		},{
			name : 'High Ethics',
			val : 0
		},];

	}

	$scope.setColaborador = function (item) {
		$scope.resetForm();

		$scope.answers.profile.ID = item.ID;
		$scope.answers.profile.name = item.display_name;
		$scope.answers.profile.email = item.user_email;
		$scope.answers.competences = item.competences;
		$scope.last_update = item.last_update ? new Date(item.last_update) : null;

		if (item.userRoles) {

			for(var key in item.userRoles){
				if (item.userRoles[key] && key === 'experience') {
					if (!item.userRoles[key][0]) {
						$scope.answers.profile[key] = null;
					}
					else {

						$scope.answers.profile[key] = item.userRoles[key][0].split(', ').map(function (elem) {
							if (elem) {

								let val1 = elem.slice( 0, elem.indexOf(' (')  );
								let val2 = elem.slice( elem.indexOf('(')+1, elem.lastIndexOf(")")  );

								return {
									"segment_experience" : val1 ,
									"experience": val2
								}
							}
							return false;
						}).filter(function (elem) {
							return elem != false;
						})
					}
				}
				else {
					$scope.answers.profile[key] = item.userRoles[key][0];
				}
			}
		}

		$timeout( async function () {

			if (!$scope.answers.profile.appraiser_name) $scope.answers.profile.appraiser_name = JSON.parse(localStorage.getItem("oAuth"))["user_display_name"];

			if ($scope.answers.profile.email && $scope.perfilEnable('administrator')) {
				let resultIsImport = await API.post("import_competences_from_last_year", { "email" : $scope.answers.profile.email, "existImport" : 1 });

				if (resultIsImport.data && resultIsImport.data.status == 400) {
					return;
				}

				$scope.existImport = Boolean(resultIsImport);
			}

			$scope.get_180_assessment();
			$scope.getAllFieldsProfile();

			$scope.formItens = [];

			$scope.$apply();


		});
	}

	$( "#project" ).autocomplete({
		minLength: 0,
		source: async function(request, response) {
			$scope.loadName = true;

			let jsonParams = {'search' : $("#project").val(), 'year' : $scope.year_selected};

			let params = Object.entries(jsonParams).map(e => e.join('=')).join('&');


			let result = await API.get("users?"+params, null, response);
			$scope.loadName = false;

			$scope.$apply()
		},
		select: function( event, ui ) {

			$scope.setColaborador(ui.item);

			if ( ui.item.userRoles.hasOwnProperty("manage_name") ) {
				$scope.get_competences_itens( ui.item.userRoles.manage_name[0] );
			}

			initPopover();
		}
	})
	.focus(function (argument) {
		if ($(this).val()) {

			$scope.resetForm();
			$scope.$apply();
		}

	})
	.autocomplete( "instance" )._renderItem = function( ul, item ) {
		let campos = ["current_position", "country", "city", "state_or_province"];
		let infouser = [];

		for(var key in item["userRoles"]){
			if ( campos.includes(key) ) {
				infouser.push( item["userRoles"][key][0] );
			}
		}

		infouser = infouser.filter(function (el) {
			return el;
		}).join(', ');

		return $( "<li>" )
		.append( "<div><strong>" + item.display_name + "</strong><br>" + infouser +"</div>" )
		.appendTo( ul );
	};

	$scope.isDev = function () {
		return window.location.href.includes("localhost") || window.location.href.includes("?homolog");
	}

	$scope.getColor = function (nums) {
		let num = parseFloat(nums);

		if (num <= 2) return { 'color' : '#fff', 'backgroundColor' : 'red' };
		if (num > 2 && num <= 4) return { 'color' : '#0000', 'backgroundColor' : 'yellow' };
		if (num > 4) return { 'color' : '#fff', 'backgroundColor' : 'green' };

		return { 'color' : '#fff', 'backgroundColor' : 'green' };

	}

	$scope.setFormItem = function (id) {
		$scope.menuId = id;

		$('.collapse').collapse('hide').delay(350).promise().done(function () {
			$('#menu-'+$scope.menuId ).collapse("show");
		});

		$scope.getAllFieldsProfile(); // atualiza resumo dos campos

	}

	$scope.nextPage = function (id) {
		$('.collapse').collapse('hide');

		console.log(id, $scope.menuId)

		if (id) {
			$('#menu-'+id ).collapse("show")
			$scope.menuId=id; return
		};
		$('#menu-'+$scope.menuId ).collapse("show")
		$scope.menuId++;

	}
	$scope.prevPage = function (id) {

		$('.collapse').collapse('hide');

		console.log(id, $scope.menuId)
		if ($scope.menuId > 0)  {

			if (id != undefined) {
				$('#menu-'+id ).collapse("show");
				$scope.menuId=id; return
			};

			$('#menu-'+$scope.menuId ).collapse("show")
			$scope.menuId--;

		};
	}

	$scope.addAnswers = function (answers_edit, slug) {
		var competences = angular.copy(answers_edit["competences"][slug]);

		competences["slug"] = slug;
		competences["competence_name"] = $scope.getNameFieldBySlug( slug );
		competences["competence_selected"] = competences[slug];
		

		if (!!!competences["edited"]) {
			competences["ID_temp"] = new Date().getTime();
			$scope.answers.competences.push(  competences );
		}
		else {
			$scope.answers.competences = $scope.answers.competences.map(function (question) {

				if( question["ID"] && question["ID"] == competences["ID"]){
					question = competences;
				}

				else if (question["ID_temp"] && question["ID_temp"] === competences["ID_temp"]) {
					question = competences;

				}
				else if (question[slug] && question[slug] === competences[slug]) {
					question = competences;

				}

				return question;

			})	

		}
		
		answers_edit["competences"][slug] = null;
		competences["edited"] = false;
	}

	$scope.getNameFieldBySlug = function (slug) {
		return $scope.formItens.filter( function (formItem) {
			return formItem.slug === slug;
		} )[0]["nameForm"]
	}

	$scope.applyChange = function () {
		setTimeout(function () {
			$scope.$apply()
		}, 500)
	}

	$scope.editAnswers = function (answer, answers_edit, slug, id) {
		if (answer) {
			answer["edited"] = true;
			answer["expected"] = Number( answer["expected"] );
			answer["priority"] = Number( answer["priority"] );
			answer["reached"] = Number( answer["reached"] );

			answers_edit["competences"][slug] = angular.copy( answer );
			answers_edit["competences"][slug][slug] = answers_edit["competences"][slug]["competence_selected"];
			answers_edit["competences"][slug]["edited"] = true;

		}
	}

	$scope.deleteAnswers = async function (answer,slug) {
		if (answer && confirm('confirm delete?')) {

			if (answer.hasOwnProperty("ID")) {

				let result = await API.post('delete_competence', { 'ID' : answer["ID"] })

				if (!result.error) {

					$scope.answers.competences = $scope.answers.competences.filter(function (question) {
						return question["ID"] !== answer["ID"];
					});

					API.alertar_sucesso(result.data);
				}

				$scope.$apply();
			}

			else {
				$scope.answers.competences = $scope.answers.competences.filter(function (question, k, a) {
					return question["ID_temp"] != answer["ID_temp"];
				});
			}


		}
	}

	$scope.addExperience = function (answers_edit,slug) {
		if (answers_edit[slug]["experience"]  ) {
			if (!$scope.answers.profile.experience ) $scope.answers.profile.experience = [];

			$scope.answers.profile.experience.push(answers_edit[slug]["experience"]);
			answers_edit[slug]["experience"] = {};
		}
	}

	$scope.deleteExperience = function (experience,slug) {
		if (experience && confirm('confirm delete?')) {
			$scope.answers.profile.experience = $scope.answers.profile.experience.filter(function (experience2) {
				return experience !== experience2;
			})	
		}
	}

	$scope.getAllFieldsProfile = function () {

		let fieldsProfile = [];


		for( field in $scope.answers.profile ){
			let profileField = angular.copy( $scope.answers.profile[field] );

			if (angular.isArray(profileField)) {
				
				profileField = profileField.map(function(item1){
					return item1.segment_experience + " ("+ item1.experience +")"
				}).toString().replace(/,+/g, ', ');

			}

			if (field == "manage_name" && profileField) {

				profileField = $scope.managers.filter(function (manager) {
					return manager.user_nicename === profileField;
				})[0]

				if (profileField && profileField.hasOwnProperty('display_name')) {
					profileField = profileField["display_name"];
				}

				else {
					profileField = "";
				}

			}

			fieldsProfile.push( { 
				name : field.replace(/_+/g, ' ').replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); }), 
				val : profileField
			} )
		}

		$scope.resumeAllFieldsProfile = fieldsProfile;


		return fieldsProfile;
	}

	$scope.get_competences_itens = async function (idManager) {

		$scope.formItens = [];

		$scope.formItens = await API.get('competences_itens?idManager='+idManager);

		let initObj = {};
		$scope.formItens.map(function (elem) {
			 initObj[elem.slug] = {}; // init objetos
			})

		$scope.answers_edit["competences"] = initObj;

		$scope.$apply();
		initPopover();
	}

	$scope.create_user_and_competence =  async function (profile) {
		profile.user_name = profile.profile.name;
		profile.email = profile.profile.email;

		if (profile.profile.hasOwnProperty("ID")) {
			profile.ID = profile.profile["ID"];
		}

		let result = await API.post('create_user_and_competence', profile);

		if (!result.error) {
			API.alertar_sucesso('Sucess!')
		}
	}

	$scope.save = async function (infos) {
		await $scope.create_user_and_competence(infos);
		await $scope.insert_update_assessment();
	}

	$scope.get_years_available = function () {	
		let yearInit = [];
		let current_year = new Date().getFullYear();

		yearInit.push( current_year-1, current_year, current_year+1 );

		if (yearInit[0] == 2018) {

			yearInit.shift();

			yearInit.push( current_year+2 );
		}

		$scope.years_available = yearInit.map(function (elem) {
			return {
				year : elem,
				isCurrent : elem === current_year,
			}
		})

	}

	$scope.set_year_selected = async function (year) {

		$scope.year_selected = year;
		$scope.resetForm();
		
		if ($scope.perfilEnable('subscriber')) {

			$scope.loadName = true;


			let jsonParams = {'search' : '', 'year' : $scope.year_selected};

			let params = Object.entries(jsonParams).map(e => e.join('=')).join('&');

			
			let result = await API.get("users?"+params, null);


			$scope.loadName = false;

			$scope.setColaborador( result[0] );
			$scope.get_competences_itens( $scope.answers.profile.manage_name );

			$scope.$apply()
		}
	}

	$scope.importFromLastYear =  async function (email) {
		if (email && $scope.perfilEnable('administrator')) {

			let result = await API.post("import_competences_from_last_year", { "email" : email });

			if (result.error) {
				return false;
			}

			if (result.length) {
				let strResult = result.map(function (elem, index) {
					return (index+1) +" - "+elem.competence_name+": "+ elem.competence_selected;
				}).join(' <br>');

				if (confirm("Import? \n" + strResult.replace(/<br\s*\/?>/gi, '\n') )) {

					$scope.answers.competences = $scope.answers.competences.concat(result);
					await $scope.save( $scope.answers );
					API.alertar_sucesso("Imported! <br>" + strResult);
				}

			} else {
				API.alertar_sucesso("Nothing to import");
			}

			$scope.$apply();
		}
	}

	$scope.get_180_assessment = async function () {

		if ($scope.answers.profile.hasOwnProperty("ID")) {

			$scope.assessment = await API.get("assessment?userID="+ $scope.answers.profile.ID);
			let removeFieldsAssessment = ["ID", "ID_user", "date_created"];

			if (angular.isArray($scope.assessment) && $scope.assessment.length) {
				$scope.assessment = $scope.assessment[0];
			}

			if ($scope.assessment && $scope.assessment.hasOwnProperty("ID")) {

				$scope.graphics = [];
				for(var key in $scope.assessment){
					if (!removeFieldsAssessment.includes(key)) {
						$scope.graphics.push({
							name: key,
							val: Number($scope.assessment[key])
						})
					}
				}
			}

			$scope.$apply();
		}
	}

	$scope.insert_update_assessment = async function () {

		let data = [];

		$scope.graphics.map(function (graphic) {
			data[graphic.name] = Number(graphic.val)
		})

		data["ID_user"] = $scope.answers.profile.ID;
		data = Object.assign({}, data);

		if ($scope.assessment.hasOwnProperty("ID")) {
			data["ID"] = $scope.assessment.ID;
		}


		let result = await API.post("insert_update_assessment", data);

		if (!result.error) {
			$scope.get_180_assessment();
		}

		$scope.$apply();
	}

	$scope.get_managers = async function () {
		$scope.managers = await API.get("managers");
		$scope.$apply();
	}

	$rootScope.perfilEnable = function (name) {
		let isIxion = false;
		if (name == 'subscriber') {
			isIxion = $scope.perfilName.includes('ixion'); // verifica tb se é ixion
		}

		return Boolean($scope.perfilName.includes(name) + isIxion);
	}

	$scope.init();
}).filter('filterBy',function(){
	return function(input, menuSlug)
	{	
		if (input && angular.isArray(input)) {
			input = input.filter(function (elem) {
				return elem.slug === menuSlug;
			})
		}

		return input;
	}
});


$(document).ready(function () {
	$(".nav-tabs .disabled, .nav-tabs .disabled a ").on("click", function(e) {
		e.preventDefault();
		return false;
	});

	setTimeout(function () {
		initPopover();
	}, 1000);
	

});


function initPopover() {

	
	$('[data-toggle="tooltip"]').tooltip(); 
	$('[data-toggle="popover"]').popover({html:true, placement: "top"}).click(function (e) {
		e.preventDefault();
	});
	

}