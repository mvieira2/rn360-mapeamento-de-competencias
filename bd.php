<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

define('JWT_AUTH_SECRET_KEY', 'Xub4x0OIUXF8zBEsU2Ukp07q7DkYlMO97q4thJ32g90WB4');
define('JWT_AUTH_CORS_ENABLE', true);


// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
//define('DB_NAME', 'skf_competence_mapping');
define('DB_NAME', 'skf_cm');

/** Usuário do banco de dados MySQL */
//define('DB_USER', 'warmind');
define('DB_USER', 'skf_cm_powerbi');

/** Senha do banco de dados MySQL */
//define('DB_PASSWORD', 'Wh4k3upgu4rd1an');
define('DB_PASSWORD', '@h6mY2YZBZbJ9zUP');

/** Nome do host do MySQL */
//define('DB_HOST', 'localhost');
define('DB_HOST', 'skf-cm.mysql.uhserver.com');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'RidKso_WO`UjR%rK#[p+NR%/*[k#(mG,?5R,b`jn~5fw4=MMPG<:&KfY9<+ ~r#P');
define('SECURE_AUTH_KEY',  'j~4$kTGD$_A}OR8/q>xzirA*7r;|E!RiG|_BB#;f>S.$p`TPTs~.e{^4d}5|~bev');
define('LOGGED_IN_KEY',    '.f$.DXj9L{3r[wYGF<JS-)V)>EUbz3hIr1P#/%.$oHQ:/ Bmf^8YW;:G*e.G@v|<');
define('NONCE_KEY',        'MZS?8n&*MQ2HaNpQU;jaBnO!|zK5hvS_MAE{g,0B<V99<Cy3$b=y!3WiM+JM8(6+');
define('AUTH_SALT',        'it`XsNt%5w$lX[Y{zn;!THAnQ>P}T/D56)#bqtNUeh{FWw[lI]d>v_rFG:6GGH|0');
define('SECURE_AUTH_SALT', 'acNN2HvZIZOJ9*.KtlMG=XW]M~?Mx]-JXHb<zfi6?4Xu%/v3yA7f93c/?Tko>^=w');
define('LOGGED_IN_SALT',   '@cYP,NO`%%gef6)55|%dedRS]OZ)|`T{cT#Kw0DbJ^Qono6g7W3V/?nUq|rd8#J!');
define('NONCE_SALT',       'gu3U8b+(VGPl9n<V_*V_K#;~)&^:.2~3LvtO<YMKK6c&uO*99}Txy`}.GgT4([Ji');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
