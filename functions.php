<?php 

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


add_action( 'after_setup_theme', 'installTheme' );
function installTheme(){
    load_theme_textdomain( 'competencesmapping', get_template_directory() . '/languages' );
}

add_filter( 'locale', 'my_theme_localized' );
function my_theme_localized( $locale )
{
  if ( isset($_GET['lang'])  ) { // logado no wordpress
    return $_GET['lang'];
}
else if(get_user_meta( get_current_user_id(), 'locale', true )){

    return get_user_meta( get_current_user_id(), 'locale', true );
}
else {

    return $locale;
}
}




require 'api/endpoints.php';

add_role(
    'manager',
    __( 'Manager (gestor)' ,'competencesmapping'),
    array(
        'read'         => true,  // Pode ler
        'edit_posts'   => true,  // Pode editar posts
        'delete_posts' => true, // Não pode apagar posts
        'sorrir'       => true,  /// Pode sorrir também (cap personalizada)
    )
);

remove_role( 'ajudante' );
remove_role( 'ixios' );
add_role(
    'ixion',
    __( 'Ixion' ,'competencesmapping'),
    array(
        'read'         => true,  // Pode ler
        'edit_posts'   => true,  // Pode editar posts
        'delete_posts' => true, // Não pode apagar posts
        'sorrir'       => true,  /// Pode sorrir também (cap personalizada)
    )
);

if ( ! function_exists( 'user_fields' ) ) :

  function user_fields( $field_user ) {
    $field_user['appraiser_name'] = __( 'Appraiser Name' ,'competencesmapping');
    $field_user['document_number'] = __( 'Document number' ,'competencesmapping');
    $field_user['current_position'] = __( 'Current Position' ,'competencesmapping');
    $field_user['time_position'] = __( 'Time Position' ,'competencesmapping');
    $field_user['skf_employee'] = __( 'Skf employee?' ,'competencesmapping');
    $field_user['mother_tongue'] = __( 'Mother Language' ,'competencesmapping');
    $field_user['experience'] = __( 'Experiences' ,'competencesmapping');
    $field_user['manage_name'] = __( 'Manager Name' ,'competencesmapping');
    $field_user['screening_date'] = __( 'Screening Date' ,'competencesmapping');
    $field_user['main_global_customer_contact'] = __( 'Main Global Customer contact' ,'competencesmapping');
    $field_user['phone'] = __( 'Phone' ,'competencesmapping');
    $field_user['country'] = __( 'Country' ,'competencesmapping');
    $field_user['city'] = __( 'City' ,'competencesmapping');
    $field_user['state_or_province'] = __( 'State or Province' ,'competencesmapping');
    $field_user['pmp'] = __( 'PMP' ,'competencesmapping');

    return $field_user;
}
add_filter('user_contactmethods','user_fields', 10, 1);

endif;



function get_rand()
{   
    //return rand();
	return wp_get_theme()->get( 'Version' );
}


